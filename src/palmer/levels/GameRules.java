package palmer.levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import palmer.blocks.Block;
import palmer.blocks.BlockBomb;
import palmer.blocks.BlockWater;
import palmer.entity.Entity;
import palmer.entity.NPC;
import palmer.entity.Player;
import palmer.gui.BlockSelector;
import palmer.main.GameEvents;
import palmer.main.GameSettings;
import palmer.main.Main;
import palmer.res.Audio;
import palmer.res.LoadImages;
import palmer.tools.Tool;

public abstract class GameRules implements Level {
	// SETTINGS
	public final int PLAYER_SPEED = 2;
	// STUFF
	int area = 1;
	public String version = "1";
	public int jumph = 2;
	public Block cust_Air = Block.air;
	public Block cust_Grass = Block.grass;
	public Block cust_Dirt = Block.dirt;
	public Block cust_Stone = Block.stone;
	public Block selb = cust_Stone;
	public Tool selt = null;
	public int tempwatercount;
	public int twctimer;
	public LevelType CurrentLevelType;
	public int sely, selx;
	// The array where all the blocks are stored
	public Block blocks[][] = new Block[17][17];
	Entity entitys[] = new Entity[20];
	Block selblocks[][] = new Block[17][17];
	Block menblocks[][] = new Block[7][7];
	BlockBomb bomb;
	BlockWater water;
	// The array where all the blocks are stored
	public NPC npc;
	// Variables for the player
	public Player player = new Player();
	// Variable for the Block Selector
	public BlockSelector bs = new BlockSelector();
	BlockSelector mbs = new BlockSelector();
	LoadImages li = new LoadImages();
	public BufferedImage menub;
	public BufferedImage menuback;
	// Variables for the bomb
	public boolean lavaRemove;
	public boolean waterRemove;
	// Boolean variable for whether the player is on the ground or not
	public boolean g;
	// Variable for when the player is going left or right
	public boolean lgo, rgo;
	public boolean menu;
	private boolean GameOver;
	public boolean waterUpdate;

	public GameRules() {
	}

	// Everything is drawn in the method
	public void draw(Graphics g) {

	}// end of draw(Graphics g)

	// all special blocks are updated here and the player is moved here
	public void update() {
		tempwatercount = 0;
		try {
			if (Main.rloop.in == null)
				return;

			if (!Main.rloop.in.up.getIsPressed() && player.g == true)
				player.tempy = 0;

			if ((player.y) % Main.rloop.TEXTURE_SIZE_Y != 0 && g == true)
				player.y -= Main.rloop.TEXTURE_SIZE_Y / 16;

			// move player right

			if (Main.rloop.in.right.getIsPressed()) {
				if (player.x / Main.rloop.TEXTURE_SIZE_Y != 14) {
					if (!player.g) {
						if (!blocks[player.x / Main.rloop.TEXTURE_SIZE_X + 1][player.y
								/ Main.rloop.TEXTURE_SIZE_Y + 1].solid) {
							player.x += Main.rloop.TEXTURE_SIZE_X / 16;
						}
					} else {
						if (!blocks[player.x / Main.rloop.TEXTURE_SIZE_X + 1][player.y
								/ Main.rloop.TEXTURE_SIZE_Y].solid) {
							player.x += Main.rloop.TEXTURE_SIZE_X / 16;
						}
						if (blocks[player.x / Main.rloop.TEXTURE_SIZE_X + 1][player.y
								/ Main.rloop.TEXTURE_SIZE_Y].id == 19) {
							player.x += Main.rloop.TEXTURE_SIZE_X;
							player.y -= Main.rloop.TEXTURE_SIZE_Y;
						}

					}
				}
			}
			// move player left
			if (Main.rloop.in.left.getIsPressed()) {
				if (player.x / Main.rloop.TEXTURE_SIZE_X != 0) {

					if (!player.g) {
						if (!blocks[player.x / Main.rloop.TEXTURE_SIZE_X][player.y
								/ Main.rloop.TEXTURE_SIZE_Y + 1].solid) {
							player.x -= Main.rloop.TEXTURE_SIZE_X / 16;
						}
					} else {
						if (!blocks[player.x / Main.rloop.TEXTURE_SIZE_X][player.y
								/ Main.rloop.TEXTURE_SIZE_Y].solid) {
							player.x -= Main.rloop.TEXTURE_SIZE_X / 16;
						}
						if (blocks[player.x / Main.rloop.TEXTURE_SIZE_X + 1][player.y
								/ Main.rloop.TEXTURE_SIZE_Y].id == 18) {
							player.x -= Main.rloop.TEXTURE_SIZE_X;
							player.y -= Main.rloop.TEXTURE_SIZE_Y;
						}
					}
				}
			}
			// make player jump
			if (Main.rloop.in.up.getIsPressed()
					&& player.tempy != Main.rloop.TEXTURE_SIZE_Y * 2) {
				if (player.tempy < Main.rloop.TEXTURE_SIZE_Y * jumph) {
					if (!blocks[player.x / Main.rloop.TEXTURE_SIZE_X][player.y
							/ Main.rloop.TEXTURE_SIZE_Y].solid) {
						player.y -= Main.rloop.TEXTURE_SIZE_Y / 16;
						player.tempy += Main.rloop.TEXTURE_SIZE_Y / 16;
					}

				}
				if (player.tempy >= Main.rloop.TEXTURE_SIZE_Y * jumph)
					Main.rloop.in.up.toggleIsPressed(false);

			}
			// make player fall
			if (!player.g) {
				if (!Main.rloop.in.up.getIsPressed()) {
					if (player.tempy2 < Main.rloop.TEXTURE_SIZE_Y) {
						player.y += Main.rloop.TEXTURE_SIZE_Y / 8;
						player.tempy += Main.rloop.TEXTURE_SIZE_Y / 8;
					}
				}
			}
			if (!blocks[(player.x + 2) / Main.rloop.TEXTURE_SIZE_X][player.y
					/ Main.rloop.TEXTURE_SIZE_Y + 1].solid
					&& !blocks[(player.x - 2) / Main.rloop.TEXTURE_SIZE_X + 1][player.y
							/ Main.rloop.TEXTURE_SIZE_Y + 1].solid) {
				player.g = false;
			}
			if (blocks[(player.x + 2) / Main.rloop.TEXTURE_SIZE_X][player.y
					/ Main.rloop.TEXTURE_SIZE_Y + 1].solid
					|| blocks[(player.x - 2) / Main.rloop.TEXTURE_SIZE_X + 1][player.y
							/ Main.rloop.TEXTURE_SIZE_Y + 1].solid) {
				player.g = true;
				player.tempy2 = 0;
				player.tempy = 0;
			}
			if (GameSettings.HQWater) {
				if (++BlockWater.WaterTexTimer > 40) {
					water = (BlockWater) Block.water;

					water.swap();
					BlockWater.WaterTexTimer = 0;
				}
			}

			for (int x = 0; x < 16; x++) {
				for (int y = 0; y < 16; y++) {
					if (blocks[x][y].id == 6) {
						blocks[x][y] = new BlockWater(-6);
					}
					if (blocks[x][y].id == 6) {
						tempwatercount++;
						if ((blocks[x][y - 1].id != 6 && blocks[x][y + 1].id == 6)
								&& (blocks[x + 1][y].id != 6 || blocks[x - 1][y].id != 6))
							blocks[x][y] = cust_Air;
					}

					// code for bomb
					if (blocks[x][y].id == 7) {
						blocks[x][y] = new BlockBomb(-7).setSolid(
								Block.bomb.solid)
								.setTexture(Block.bomb.Texture);
					}
					if (blocks[x][y].id == -7) {
						bomb = (BlockBomb) blocks[x][y];
						if (--bomb.fuse <= 0) {
							bomb.use(x, y);
							bomb.fuse = 0;
							bomb = null;
							blocks[x][y] = cust_Air;
						}

					}
					if (player.x / Main.rloop.TEXTURE_SIZE_X >= 14
							&& (CurrentLevelType == LevelType.Normal || CurrentLevelType == LevelType.Sand)) {
						if (new Random().nextBoolean())
							init(LevelType.Normal);
						else {
							init(LevelType.Sand);
						}
						player.x = 2 * Main.rloop.TEXTURE_SIZE_X;
						Main.cSX++;
					} else if (player.x / Main.rloop.TEXTURE_SIZE_X >= 14
							&& CurrentLevelType == LevelType.Cave) {
						init(LevelType.Cave);
						player.x = 2 * Main.rloop.TEXTURE_SIZE_X;
						Main.cSX++;
					}
					if (player.x / Main.rloop.TEXTURE_SIZE_X <= 1
							&& (CurrentLevelType == LevelType.Normal || CurrentLevelType == LevelType.Sand)) {
						if (new Random().nextBoolean())
							init(LevelType.Normal);
						else {
							init(LevelType.Sand);
						}
						Main.cSX--;
						player.x = 13 * Main.rloop.TEXTURE_SIZE_X;
					} else if (player.x / Main.rloop.TEXTURE_SIZE_X <= 1
							&& CurrentLevelType == LevelType.Cave) {
						init(LevelType.Cave);
						player.x = 13 * Main.rloop.TEXTURE_SIZE_X;
						Main.cSX--;
					}
					if (player.y / Main.rloop.TEXTURE_SIZE_Y == 15) {
						init(LevelType.Cave);
						blocks[player.x / Main.rloop.TEXTURE_SIZE_X][0] = cust_Air;
						player.y = 1 * Main.rloop.TEXTURE_SIZE_Y;
						Main.cSY++;
					}
					// trigger nuke
					if (blocks[x][y].id == 11) {
						Audio.requestPlaySound("Nuke");
						GameEvents.fire("nuke");
					}

					// liquid flow and lava burn
					if (waterUpdate) {
						if (!Main.Floodflow) {
							if (++BlockWater.WaterFlowTimer > 20) {
								normFlow(x, y);
								BlockWater.WaterFlowTimer = 0;
							}
							normFlowL(x, y);
							Burn(x, y);
						}
					}
					// unused
					if (Main.Floodflow) {
						floodFlow(y, y);

					}
					// remove water
					if (blocks[x][y].id == 27) {
						GameEvents.fire("antiWater", x, y);

					}

					// remove doors
					if (blocks[x][y].id == 25) {
						removeDoors(x, y);
					}

					// map reset
					if (blocks[x][y].id == 13) {
						GameEvents.fire("reset");
					}
					// create tree
					if (blocks[x][y].id == 14) {
						GameEvents.fire("instaTree", x, y);

					}
					// create door

					// create door

					// create house
					if (blocks[x][y].id == 20) {
						GameEvents.fire("instaHouse", x, y);

					}
				}
				if (lavaRemove) {
					lavaRemove();
					lavaRemove = false;
				}
				if (waterRemove) {
					waterRemove();
					waterRemove = false;
				}

			}
			if (++twctimer > 10) {
				Main.rloop.watercount = tempwatercount;
				twctimer = 0;
			}
		} catch (Exception e) {
			return;
		}

	}

	// unused
	public void colision() {

	}

	public void instaCave(int x, int y) {
		for (int fx = 0; fx < 5; fx++) {
			for (int fy = 0; fy < 5; fy++) {
				blocks[x + fx][y - fy] = Block.stoneInside;
			}

		}

		for (int fx = 0; fx < 5; fx++) {
			blocks[x + fx][y - 5] = Block.stone;
		}
		for (int fy = 0; fy < 5; fy++) {
			blocks[x + 4][y - fy] = Block.stone;
		}
		blocks[x - 1][y] = Block.stone;
	}

	// create level
	public void menInit() {
		menblocks[0][0] = Block.stone;
		menblocks[0][1] = Block.dirt;
		menblocks[0][2] = Block.grass;
		menblocks[0][3] = Block.cloud;
		menblocks[0][4] = Block.water;
		menblocks[0][5] = Block.bomb;
		menblocks[1][0] = Block.log;
		menblocks[1][1] = Block.branches;
		menblocks[1][2] = Block.nuke;
		menblocks[1][3] = Block.slimeBlock;
		menblocks[1][4] = Block.world;
		menblocks[1][5] = Block.tree;
		menblocks[2][0] = Block.bricks;
		menblocks[2][1] = Block.doorL;
		menblocks[2][2] = Block.doorR;
		menblocks[2][3] = Block.brickStairsR;
		menblocks[2][4] = Block.brickStairsL;
		menblocks[2][5] = Block.instaHouse;
		menblocks[3][0] = Block.bricksInside;
		menblocks[3][1] = Block.ladder;
		menblocks[3][2] = Block.lava;
		menblocks[3][3] = Block.brickDoorL;
		menblocks[3][4] = Block.brickDoorR;
		menblocks[4][4] = Block.warpPad;
		menblocks[4][5] = Block.radio;
		menblocks[5][0] = Block.toolChest;
		menblocks[5][1] = Block.deadBranches;
		menblocks[5][2] = Block.sand;
		for (int x = 0; x < 6; x++) {
			for (int y = 0; y < 6; y++) {
				if (menblocks[x][y] == null) {
					menblocks[x][y] = Block.menuEmpty;
				}
			}
		}

	}

	public abstract GameRules init(LevelType lt);

	public GameRules init() {
		menInit();
		npc = new NPC();

		try {
			menub = li.getTex("Menu");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			menuback = li.getTex("MenuBack");
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int count = 0; count < 16; count++) {
			blocks[count][15] = cust_Stone;

		}
		for (int count = 0; count < 16; count++) {
			blocks[count][14] = cust_Dirt;

		}
		for (int count = 0; count < 16; count++) {
			blocks[count][13] = cust_Grass;

		}
		// instaCloud(6,3);

		for (int countout = 0; countout < 16; countout++) {
			for (int countin = 0; countin < 16; countin++) {
				if (blocks[countout][countin] == null)
					blocks[countout][countin] = cust_Air;
			}
		}
		return this;
	}

	// used for changing blocks from other classes
	public void changeBlock(int x, int y, Block b) {
		if (x < 17 && y < 16) {
			blocks[x][y] = b;
		} else {
			Main.con.print("[Error]Invalid Block Location");
			Main.rloop.con = "[Error]Invalid Block Location";
		}

	}

	// used for setting the Block selector when the mouse moves
	public void setSelector(int x, int y) {
		bs.x = x * Main.rloop.TEXTURE_SIZE_X;
		bs.y = y * Main.rloop.TEXTURE_SIZE_Y;

	}

	public void instaCloud(int x, int y) {
		blocks[x][y] = Block.cloud;
		blocks[x + 1][y] = Block.cloud;
		blocks[x + 2][y] = Block.cloud;
		blocks[x + 3][y] = Block.cloud;
		blocks[x + 4][y] = Block.cloud;
		blocks[x + 1][y - 1] = Block.cloud;
		blocks[x + 2][y - 1] = Block.cloud;
		blocks[x + 3][y - 1] = Block.cloud;
	}

	// makes the water flow
	public void normFlow(int countout, int countin) {
		if (blocks[countout][countin].id == -6) {
			BlockWater bw = (BlockWater) blocks[countout][countin];
			if (bw.waterCount <= 0) {

				blocks[countout][countin] = Block.air;
			}
			if (!blocks[countout][countin + 1].solid
					&& (bw.waterCount > 0 && blocks[countout][countin + 1].id != 6)) {
				blocks[countout][countin + 1] = new BlockWater(-6);
				bw.waterCount--;
			}

			else if (countout != 15 && countout != 0) {
				if (!blocks[countout - 1][countin].solid
						&& (bw.waterCount > 0 && blocks[countout - 1][countin + 1].id != 6)) {
					blocks[countout - 1][countin] = new BlockWater(-6);
					bw.waterCount--;
				}

				if (!blocks[countout + 1][countin].solid
						&& (bw.waterCount > 0 && blocks[countout + 1][countin + 1].id != 6)) {
					blocks[countout + 1][countin] = new BlockWater(-6);
					bw.waterCount--;

				}
			}

		}
	}

	// unused
	public void floodFlow(int countout, int countin) {
		if (blocks[countout][countin].id == 6) {
			System.out.println("Hey");
			if (blocks[countout][countin + 1].id == 0) {
				blocks[countout][countin + 1] = Block.water;
			} else if (countout != 15 && countout != 0) {
				if (blocks[countout - 1][countin].id == 0)
					blocks[countout - 1][countin] = Block.water;
				if (blocks[countout + 1][countin].id == 0)
					blocks[countout + 1][countin] = Block.water;

			}
		}

	}

	// used for bomb
	public void Boom(int x, int y) {
		blocks[x][y] = cust_Air;
		blocks[x - 1][y - 1] = cust_Air;
		blocks[x + 1][y + 1] = cust_Air;
		blocks[x + 1][y - 1] = cust_Air;
		blocks[x - 1][y + 1] = cust_Air;
		blocks[x - 1][y] = cust_Air;
		blocks[x + 1][y] = cust_Air;
		blocks[x][y - 1] = cust_Air;
		blocks[x][y + 1] = cust_Air;
		blocks[x + 2][y - 1] = cust_Air;
		blocks[x + 2][y] = cust_Air;
		blocks[x + 2][y + 1] = cust_Air;
		blocks[x - 2][y - 1] = cust_Air;
		blocks[x - 2][y] = cust_Air;
		blocks[x - 2][y + 1] = cust_Air;
		blocks[x - 1][y - 2] = cust_Air;
		blocks[x][y - 2] = cust_Air;
		blocks[x + 1][y - 2] = cust_Air;
		blocks[x - 1][y + 2] = cust_Air;
		blocks[x][y + 2] = cust_Air;
		blocks[x + 1][y + 2] = cust_Air;

	}

	// this nukes the map
	public void nuke() {
		Main.con.print("[Blocks][Nuke]Boom!");
		Main.rloop.con = "[Blocks][Nuke]Boom!";
		for (int countout1 = 0; countout1 < 16; countout1++) {
			for (int countin1 = 0; countin1 < 16; countin1++) {

				blocks[countout1][countin1] = cust_Air;
			}
		}
		GameEvents.fire("clear");
		GameOver = true;

	}

	// used the reset the map
	public void reset() {
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				blocks[x][y] = cust_Air;
			}
		}

		init();
		GameEvents.fire("clear");

	}

	public void reset(LevelType lt) {
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				blocks[x][y] = cust_Air;
			}
		}

		init(lt);
		GameEvents.fire("clear");

	}

	// creates a tree
	public void instaTree(int x, int y) {
		blocks[x][y] = Block.log;
		/*
		 * blocks[x][y - 1] = Block.log; blocks[x][y - 2] = Block.log;
		 * blocks[x][y - 3] = Block.log;
		 */
		int i;
		do {
			i = new Random().nextInt(9);
		} while (i <= 2);
		for (int count = 0; count < i; count++) {
			blocks[x][y + (-count)] = Block.log;
		}
		Block b = null;
		if (new Random().nextBoolean()) {
			b = Block.branches;
		} else {
			b = Block.deadBranches;
		}
		if (i < 6) {
			blocks[x][y - (0 + i)] = b;
			blocks[x][y - (1 + i)] = b;
			blocks[x - 1][y - (0 + i)] = b;
			blocks[x + 1][y - (0 + i)] = b;
			blocks[x + 1][y - (-1 + i)] = b;
			blocks[x - 1][y - (-1 + i)] = b;

		} else {
			blocks[x][y - (0 + i)] = b;
			blocks[x][y - (1 + i)] = b;
			blocks[x - 1][y - (0 + i)] = b;
			blocks[x + 1][y - (0 + i)] = b;
			blocks[x + 1][y - (-1 + i)] = b;
			blocks[x - 1][y - (-1 + i)] = b;
			blocks[x - 2][y - (-1 + i)] = b;
			blocks[x + 2][y - (-1 + i)] = b;

		}
		GameEvents.fire("clear");
	}

	public void instaTree(int x, int y, int state) {
		blocks[x][y] = Block.log;
		/*
		 * blocks[x][y - 1] = Block.log; blocks[x][y - 2] = Block.log;
		 * blocks[x][y - 3] = Block.log;
		 */
		int i;
		do {
			i = new Random().nextInt(9);
		} while (i <= 2);
		for (int count = 0; count < i; count++) {
			blocks[x][y + (-count)] = Block.log;
		}
		Block b = null;
		if (state == 1) {
			b = Block.branches;
		} else {
			b = Block.deadBranches;
		}
		if (i < 6) {
			blocks[x][y - (0 + i)] = b;
			blocks[x][y - (1 + i)] = b;
			blocks[x - 1][y - (0 + i)] = b;
			blocks[x + 1][y - (0 + i)] = b;
			blocks[x + 1][y - (-1 + i)] = b;
			blocks[x - 1][y - (-1 + i)] = b;

		} else {
			blocks[x][y - (0 + i)] = b;
			blocks[x][y - (1 + i)] = b;
			blocks[x - 1][y - (0 + i)] = b;
			blocks[x + 1][y - (0 + i)] = b;
			blocks[x + 1][y - (-1 + i)] = b;
			blocks[x - 1][y - (-1 + i)] = b;
			blocks[x - 2][y - (-1 + i)] = b;
			blocks[x + 2][y - (-1 + i)] = b;

		}
		GameEvents.fire("clear");
	}

	// creates a brick house
	public void instaHouse(int x, int y) {

		blocks[x][y] = Block.brickDoorL;
		blocks[x][y - 1] = Block.brickDoorL;
		blocks[x][y - 2] = Block.bricks;
		Main.li.fixStairs();
		blocks[x][y - 3] = Block.brickStairsL;
		for (int loopvar = 1; loopvar < 6; loopvar++) {
			blocks[x + loopvar][y - 3] = Block.bricks;
		}
		blocks[x + 6][y - 3] = Block.brickStairsR;
		blocks[x + 6][y - 2] = Block.bricks;
		blocks[x + 6][y - 1] = Block.bricks;
		blocks[x + 6][y] = Block.bricks;

		for (int loopvar = 0; loopvar < 3; loopvar++) {
			for (int loopvar2 = 1; loopvar2 < 6; loopvar2++) {
				blocks[x + loopvar2][y - loopvar] = Block.bricksInside;
			}
		}
		GameEvents.fire("clear");

	}

	// used to make lava flow
	public void normFlowL(int x, int y) {
		if (blocks[x][y].id == 24) {
			if (blocks[x][y + 1].id == cust_Air.id || blocks[x][y + 1].id == 24) {
				blocks[x][y + 1] = Block.lava;
				GameEvents.fire("clear");

			}

			else if (x != 15 && x != 0) {
				if (blocks[x - 1][y].id == cust_Air.id) {
					blocks[x - 1][y] = Block.lava;
					GameEvents.fire("clear");
				}

				if (blocks[x + 1][y].id < 1) {
					blocks[x + 1][y] = Block.lava;
					GameEvents.fire("clear");

				}
			}

		}

	}

	// used the make lava burn wood;
	public void Burn(int x, int y) {
		if (blocks[x][y].id == 24 && y != 0) {

			// Top
			if ((x != 0 && x != 15) && (y != 0 && y != 15)) {
				if (blocks[x][y - 1].id == 8) {
					blocks[x][y - 1] = cust_Air;
					GameEvents.fire("clear");

				} else if (blocks[x][y - 1].id == 9) {
					blocks[x][y - 1] = cust_Air;
					GameEvents.fire("clear");

				}
			}// end of Top
				// Bottom
			if ((x != 0 && x != 15) && (y != 0 && y != 15)) {
				if (blocks[x][y + 1].id == 8) {
					blocks[x][y + 1] = cust_Air;
					GameEvents.fire("clear");

				} else if (blocks[x][y + 1].id == 9) {
					blocks[x][y + 1] = cust_Air;
					GameEvents.fire("clear");

				}
			}// end of Bottom
				// Right
			if ((x != 0 && x != 15) && (y != 0 && y != 15)) {
				if (blocks[x + 1][y].id == 8) {
					blocks[x + 1][y] = cust_Air;
					GameEvents.fire("clear");

				} else if (blocks[x + 1][y].id == 9) {
					blocks[x + 1][y] = cust_Air;
					GameEvents.fire("clear");

				}
			}// end of Right
				// Left

			if ((x != 0 && x != 15) && (y != 0 && y != 15)) {
				if (blocks[x - 1][y].id == 8) {
					blocks[x - 1][y] = cust_Air;
					GameEvents.fire("clear");

				} else if (blocks[x - 1][y].id == 9) {
					blocks[x - 1][y] = cust_Air;
					GameEvents.fire("clear");

				}
			}// end of Left

		}

	}

	// used in removing doors tool
	public void removeDoors(int x, int y) {

		blocks[x][y] = cust_Air;

		if (((blocks[x][y + 1].id == 16) || (blocks[x][y + 1].id == 17))
				|| (blocks[x][y + 1].id == 22) || (blocks[x][y + 1].id == 26)) {
			blocks[x][y + 1] = cust_Air;
		}
		if (((blocks[x][y - 1].id == 16) || (blocks[x][y - 1].id == 17))
				|| (blocks[x][y - 1].id == 22) || (blocks[x][y - 1].id == 26)) {
			blocks[x][y - 1] = cust_Air;
		}
		if (((blocks[x][y - 2].id == 16) || (blocks[x][y - 2].id == 17))
				|| (blocks[x][y - 2].id == 22) || (blocks[x][y - 2].id == 26)) {
			blocks[x][y - 2] = cust_Air;
		}

	}

	// used in removing water tool
	public void removeWater(int x, int y) {
		/*
		 * blocks[x][y] = cust_Air; // Top
		 * 
		 * if (y != 0 && blocks[x][y - 1].id == 6) { blocks[x][y - 1] = new
		 * WaterRemover();
		 * 
		 * }
		 * 
		 * if (y != 15 && blocks[x][y + 1].id == 6) { blocks[x][y + 1] = new
		 * WaterRemover(); } if (x != 15 && blocks[x + 1][y].id == 6) { blocks[x
		 * + 1][y] = new WaterRemover(); } if (x != 0 && blocks[x - 1][y].id ==
		 * 6) { blocks[x - 1][y] = new WaterRemover(); }
		 * 
		 * GameEvents.fire("clear"); }
		 * 
		 * // used in removing lava tool public void removeLava(int x, int y) {
		 * blocks[x][y] = cust_Air; // Top
		 * 
		 * if (y != 0 && blocks[x][y - 1].id == 24) { blocks[x][y - 1] = new
		 * LavaRemover();
		 * 
		 * }
		 * 
		 * if (y != 15 && blocks[x][y + 1].id == 24) { blocks[x][y + 1] = new
		 * LavaRemover(); } if (x != 15 && blocks[x + 1][y].id == 24) { blocks[x
		 * + 1][y] = new LavaRemover(); } if (x != 0 && blocks[x - 1][y].id ==
		 * 24) { blocks[x - 1][y] = new LavaRemover(); }
		 * GameEvents.fire("clear");
		 * 
		 * }
		 * 
		 * // used in remove tree tool public void removeTree(int x, int y) {
		 * blocks[x][y] = cust_Air; // Top
		 * 
		 * if ((y != 0 && (blocks[x][y + 1].id == 8 || blocks[x][y + 1].id ==
		 * 9)) || blocks[x][y+ 1].id == 37) { blocks[x][y + 1] = new
		 * TreeRemover();
		 * 
		 * }
		 * 
		 * if ((y != 15 && (blocks[x][y - 1].id == 8 || blocks[x][y - 1].id ==
		 * 9))|| blocks[x][y- 1].id == 37) { blocks[x][y - 1] = new
		 * TreeRemover(); } if ((x != 15 && (blocks[x + 1][y].id == 8 ||
		 * blocks[x + 1][y].id == 9))|| blocks[x+1][y].id == 37) { blocks[x +
		 * 1][y] = new TreeRemover(); } if ((x != 0 && (blocks[x - 1][y].id == 8
		 * || blocks[x - 1][y].id == 9))|| blocks[x-1][y].id == 37) { blocks[x -
		 * 1][y] = new TreeRemover(); }
		 */
	}// end of removeTree(int x, int y)

	public void updateSelector(int x, int y) {

		// System.out.println("Hey");
		selx = x + 4;
		sely = y;
		x = x - 1;
		y = y - 4;
		if (x == 0 && y == 0) {
			selb = Block.stone;
			Main.con.print("[Block Menu]Stone Selected");
			Main.rloop.con = "[Block Menu]Stone Selected";

		} else if (x == 0 && y == 1) {
			selb = Block.dirt;
			Main.con.print("[Block Menu]Dirt Selected");
			Main.rloop.con = "[Block Menu]Dirt Selected";
		} else if (x == 0 && y == 2) {
			selb = Block.grass;
			Main.con.print("[Block Menu]Grass Selected");
			Main.rloop.con = "[Block Menu]Grass Selected";
		} else if (x == 0 && y == 3) {
			selb = Block.cloud;
			Main.con.print("[Block Menu]Cloud Selected");
			Main.rloop.con = "[Block Menu]Cloud Selected";
		} else if (x == 0 && y == 4) {
			selb = Block.water;
			Main.con.print("[Block Menu]Water Selected");
			Main.rloop.con = "[Block Menu]Water Selected";
		} else if (x == 0 && y == 5) {
			selb = Block.bomb;
			Main.con.print("[Block Menu]Bomb Selected");
			Main.rloop.con = "[Block Menu]Bomb Selected";
		} else if (x == 1 && y == 0) {
			selb = Block.log;
			Main.con.print("[Block Menu]Log Selected");
			Main.rloop.con = "[Block Menu]Log Selected";
		} else if (x == 1 && y == 1) {
			selb = Block.branches;
			Main.con.print("[Block Menu]Branches Selected");
			Main.rloop.con = "[Block Menu]Branches Selected";
		} else if (x == 1 && y == 2) {
			selb = Block.nuke;
			Main.con.print("[Block Menu]Nuke Selected");
			Main.rloop.con = "[Block Menu]Nuke Selected";

		} else if (x == 1 && y == 3) {
			selb = Block.slimeBlock;
			Main.con.print("[Block Menu]Slime Selected");
			Main.rloop.con = "[Block Menu]Slime Selected";

		} else if (x == 1 && y == 4) {
			selb = Block.world;
			Main.con.print("[Block Menu]World in a Block Selected");
			Main.rloop.con = "[Block Menu]World in a Block Selected";

		} else if (x == 1 && y == 5) {
			selb = Block.tree;
			Main.con.print("[Block Menu]InstaTree Selected");
			Main.rloop.con = "[Block Menu]InstaTree Selected";

		} else if (x == 2 && y == 0) {
			selb = Block.bricks;
			Main.con.print("[Block Menu]Bricks Selected");
			Main.rloop.con = "[Block Menu]Bricks Selected";

		} else if (x == 2 && y == 1) {
			selb = Block.doorL;
			Main.con.print("[Block Menu]Left Door Selected");
			Main.rloop.con = "[Block Menu]Left Door Selected";

		} else if (x == 2 && y == 2) {
			selb = Block.doorR;
			Main.con.print("[Block Menu]Right Door Selected");
			Main.rloop.con = "[Block Menu]Right Door Selected";

		} else if (x == 2 && y == 3) {
			selb = Block.brickStairsR;
			Main.con.print("[Block Menu]Right Brick Stairs Selected");
			Main.rloop.con = "[Block Menu]Right Brick Stairs Selected";

		} else if (x == 2 && y == 4) {
			selb = Block.brickStairsL;
			Main.con.print("[Block Menu]Left Brick Stairs Selected");
			Main.rloop.con = "[Block Menu]Left Brick Stairs Selected";

		} else if (x == 2 && y == 5) {
			selb = Block.instaHouse;
			Main.con.print("[Block Menu]InstaHouse Selected");
			Main.rloop.con = "[Block Menu]InstaHouse Selected";

		} else if (x == 3 && y == 0) {
			selb = Block.bricksInside;
			Main.con.print("[Block Menu]Inside Bricks Selected");
			Main.rloop.con = "[Block Menu]Inside Bricks Selected";

		} else if (x == 3 && y == 1) {
			selb = Block.ladder;
			Main.con.print("[Block Menu]Ladder Selected");
			Main.rloop.con = "[Block Menu]Ladder Selected";

		} else if (x == 3 && y == 2) {
			selb = Block.lava;
			Main.con.print("[Block Menu]Lava Selected");
			Main.rloop.con = "[Block Menu]Lava Selected";

		} else if (x == 3 && y == 3) {
			selb = Block.brickDoorL;
			Main.con.print("[Block Menu]Left Brick Door Selected");
			Main.rloop.con = "[Block Menu]Left Brick Door Selected";

		} else if (x == 3 && y == 4) {
			selb = Block.brickDoorR;
			Main.con.print("[Block Menu]Right Brick Door Selected");
			Main.rloop.con = "[Block Menu]Right Brick Door Selected";

		} else if (x == 4 && y == 4) {
			selb = Block.warpPad;
			Main.con.print("[Block Menu]WarpPad Selected");
			Main.rloop.con = "[Block Menu]WarpPad Selected";

		} else if (x == 4 && y == 5) {
			selb = Block.radio;
			Main.con.print("[Block Menu]Radio Selected");
			Main.rloop.con = "[Block Menu]Radio Selected";

		} else if (x == 5 && y == 0) {
			selb = Block.toolChest;
			Main.con.print("[Block Menu]ToolChest Selected");
			Main.rloop.con = "[Block Menu]ToolChest Selected";

		} else if (x == 5 && y == 1) {
			selb = Block.deadBranches;
			Main.con.print("[Block Menu]deadBranches Selected");
			Main.rloop.con = "[Block Menu]deadBranches Selected";

		} else if (x == 5 && y == 2) {
			selb = Block.sand;
			Main.con.print("[Block Menu]Sand Selected");
			Main.rloop.con = "[Block Menu]Sand Selected";

		} else if (x == 5 && y == 3) {
			selb = Block.cactus;
			Main.con.print("[Block Menu]Cactus Selected");
			Main.rloop.con = "[Block Menu]Cactus Selected";

		}

		else {
			selb = Block.menuEmpty;
			Main.con.print("[Block Menu]Nothing Selected");
			Main.rloop.con = "[Block Menu]Nothing Selected";
		}
	}

	public Block getBlockGrid(int x, int y) {
		return blocks[x][y];
	}

	public Block getBlock(int id) {
		if (id == 0) {
			return Block.air;
		} else if (id == 1) {
			return Block.cloud;
		} else if (id == 2) {
			return Block.stone;
		} else if (id == 3) {
			return Block.dirt;
		} else if (id == 4) {
			return Block.menuEmpty;
		} else if (id == 5) {
			return Block.grass;
		} else if (id == 6) {
			return Block.water;
		} else if (id == 7) {
			return Block.bomb;
		} else if (id == 8) {
			return Block.log;
		} else if (id == 9) {
			return Block.branches;
		} else if (id == 10) {
			return Block.stillWater;
		} else if (id == 11) {
			return Block.nuke;
		} else if (id == 12) {
			return Block.slimeBlock;
		} else if (id == 13) {
			return Block.world;
		} else if (id == 14) {
			return Block.tree;
		} else if (id == 15) {
			return Block.bricks;
		} else if (id == 16) {
			return Block.doorL;
		} else if (id == 17) {
			return Block.doorR;
		} else if (id == 18) {
			return Block.brickStairsR;
		} else if (id == 19) {
			return Block.brickStairsL;
		} else if (id == 20) {
			return Block.instaHouse;
		} else if (id == 21) {
			return Block.bricksInside;
		} else if (id == 22) {
			return Block.brickDoorL;
		} else if (id == 23) {
			return Block.ladder;
		} else if (id == 24) {
			return Block.lava;
		} else if (id == 26) {
			return Block.brickDoorR;
		} else if (id == 30) {
			return Block.stoneInside;
		} else if (id == 31) {
			return Block.oreInside[Block.ORE_IRON];
		} else if (id == 32) {
			return Block.oreInside[Block.ORE_GOLD];
		} else if (id == 33) {
			return Block.oreInside[Block.ORE_DIAMOND];
		} else if (id == 34) {
			return Block.warpPad;
		} else if (id == 35) {
			return Block.radio;
		} else if (id == 36) {
			return Block.toolChest;
		} else if (id == 37) {
			return Block.deadBranches;
		} else if (id == 38) {
			return Block.sand;
		} else if (id == 39) {
			return Block.cactus;
		} else if (id == -1) {
			return Block.custom;
		} else if (id == -2) {
			return Block.custom2;
		} else if (id == -3) {
			return Block.custom3;
		} else
			return Block.menuEmpty;
	}

	public void textureInit() {
		LoadImages li = new LoadImages();
		try {
			switch (version) {
			case "1":
				cust_Air.Texture = li.getTex("Block_Air");
				cust_Grass.Texture = li.getTex("Block_Grass");
				cust_Dirt.Texture = li.getTex("Block_Dirt");
				cust_Stone.Texture = li.getTex("Block_Stone");
				break;
			case "2":
				cust_Air.Texture = li.getTex("Block2_Air");
				cust_Grass.Texture = li.getTex("Block2_Grass");
				cust_Dirt.Texture = li.getTex("Block2_Dirt");
				cust_Stone.Texture = li.getTex("Block2_Stone");
				break;
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void menDraw(Graphics g) {
		if (menu) {
			// g.drawImage(menub, 160, 153, Main.rloop);
			g.drawImage(menuback, 0, 0, Main.rloop.frame.getWidth(),
					Main.rloop.frame.getHeight(), Main.rloop);
			for (int x = 0; x < 6; x++) {
				for (int y = 0; y < 6; y++) {
					g.drawImage(menblocks[x][y].Texture, x
							* Main.rloop.TEXTURE_SIZE_X + (160 / 32)
							* Main.rloop.TEXTURE_SIZE_X, y
							* Main.rloop.TEXTURE_SIZE_Y + (153 / 32)
							* Main.rloop.TEXTURE_SIZE_Y,
							Main.rloop.TEXTURE_SIZE_X,
							Main.rloop.TEXTURE_SIZE_Y, Main.rloop);

				}
			}
			g.drawImage(mbs.Texture, (selx * Main.rloop.TEXTURE_SIZE_X) - 5,
					(sely * Main.rloop.TEXTURE_SIZE_Y) + (22 / 32)
							* Main.rloop.TEXTURE_SIZE_Y,
					Main.rloop.TEXTURE_SIZE_X + (10 / 32)
							* Main.rloop.TEXTURE_SIZE_X,
					Main.rloop.TEXTURE_SIZE_Y + (1 / 8)
							* Main.rloop.TEXTURE_SIZE_Y, Main.rloop);
			GameEvents.fire("clear");
			for (int y = 0; y < 6; y++) {

			}
		}

	}

	public void createEntity(Entity e) {
		for (int count = 0; count < 20; count++) {
			if (entitys[count] == null) {
				entitys[count] = e;
				return;
			} else {
				System.out.println(count);
			}
		}
	}

	public BufferedImage getEntityTexture(int i) {
		return entitys[i].Texture;

	}

	public int getEntityX(int i) {
		return entitys[i].x;
	}

	public int getEntityY(int i) {
		return entitys[i].y;
	}

	public Entity getEntity(int i) {
		return entitys[i];
	}

	public void requestImageDraw(BufferedImage b, int x, int y, int endX,
			int endY) {
		Main.rloop.addImage(b, x, y, endX, endY);
	}

	public void lavaRemove() {
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				if (blocks[x][y].id == 24) {
					blocks[x][y] = cust_Air;
				}
			}
		}

	}

	public void waterRemove() {
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				if (blocks[x][y].id == 6) {
					blocks[x][y] = cust_Air;
				}
			}
		}

	}

	public Block[][] getBlocks() {
		return blocks;
	}

	public void PlayerTP(int x, int y) {
		player.x = Main.rloop.TEXTURE_SIZE_X * x;
		player.y = Main.rloop.TEXTURE_SIZE_Y * y;
	}
}
