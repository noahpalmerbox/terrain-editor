package palmer.levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import palmer.blocks.Block;
import palmer.entity.Entity;

public interface Level {
	public void draw(Graphics g);
	public void update();
	public void colision();
	public GameRules init();
	public void changeBlock(int x, int y, Block b);
	public void updateSelector(int x, int y);
	public void menInit();
	public Block getBlockGrid(int x, int y);
	public Block getBlock(int id);
	public void createEntity(Entity e);
	public BufferedImage getEntityTexture(int i);
	public int getEntityX(int i);
	public int getEntityY(int i);
	public Entity getEntity(int i);
	public void menDraw(Graphics g);
	

}
