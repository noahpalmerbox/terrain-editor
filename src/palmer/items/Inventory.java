package palmer.items;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import palmer.blocks.Block;
import palmer.exceptions.IncorrectItemException;
import palmer.main.Main;
import palmer.tools.Tool;

public class Inventory {
	public volatile boolean shouldRender;
	public ItemStack inv[]= new ItemStack[32];
	
	public void addItem(Item i){
		for(int count = 0;count < inv.length;count++){
			if(inv[count] != null && (inv[count].id == i.id)){
				try {
					inv[count].add(i);
					return;
				} catch (IncorrectItemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		for(int count = 0;count < inv.length;count++){
			if(inv[count] == null){
				inv[count] = new ItemStack(i, 1);
				return;
			}
			
		}
		
	
	
	}
	public void addItem(Item i, int c){
		for(int count = 0;count < inv.length;count++){
			if(inv[count] != null && (inv[count].id == i.id)){
				try {
					inv[count].add(i, c);
					return;
				} catch (IncorrectItemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		for(int count = 0;count < inv.length;count++){
			if(inv[count] == null){
				inv[count] = new ItemStack(i, c);
				return;
			}
			
		}
		
	
	
	}

	public Item getItem(int counter) throws IncorrectItemException{
		if(inv[counter] != null){
		Item i  = inv[counter].take();
		if(inv[counter].amount == 0){
			inv[counter] = null;
		}
		return i;
		}
		throw new IncorrectItemException();
		
	}
	public void removeItemStack(int i){
		inv[i] = null;
	}
	
	public void render(Graphics g){
		g.drawImage(Main.rloop.menuBack, 0, 0, null);
		for(int count = 0;count < 14;count++){
			if(inv[count] != null)
				g.drawImage(inv[count].item.Texture, count*32+30, 255, null);
		}
		for(int count = 0;count < 14;count++){
			if(inv[count+14] != null)
				g.drawImage(inv[count+14].item.Texture, count*32+30, 287, null);
		}
	}
	public void handleClick(MouseEvent e){
		if(inv[e.getX()/32-1] != null)
			if(inv[e.getX()/32-1].item.type == ItemType.Block){
				
			
				Main.rloop.lv1.selb = (Block)inv[e.getX()/32-1].item;
			}else if(inv[e.getX()/32-1].item.type == ItemType.Tool){
				System.out.println("BOGO");
				if(Main.rloop.lv1.selt == null)
				Main.rloop.lv1.selt = (Tool) inv[e.getX()/32-1].item;
				else
					Main.rloop.lv1.selt = null;
			}
		
		
		
	}
	public int hasItem(Item i){
		for(int count = 0;count < inv.length;count++){
			if(inv[count] != null){
			if(inv[count].id == i.id){
				return count;
			}
			}
		}
		return -1;
		
	}

}
