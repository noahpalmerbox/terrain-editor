package palmer.items;

import java.awt.image.BufferedImage;

import palmer.main.Main;

public class Item {
	public static Item test;
	
	
	public static void init(){
		test = new Item(40).setTexture(Main.ItemTextures[0]).setType(ItemType.Item);
	}
	public BufferedImage Texture;
	public int id;
	public ItemType type;
	public boolean isBlock;
	public Item(int id2){
		id = id2;
	}
	public Item setTexture(BufferedImage b){
		Texture = b;
		return this;
		
	}
	public Item setType(ItemType i){
		type = i;
		return this;
	}
	
}
