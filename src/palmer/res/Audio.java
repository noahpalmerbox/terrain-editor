package palmer.res;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import palmer.main.Main;

public class Audio {
	static String[]sounds = new String[10];
	public static Clip CurrentClip;
	 static AudioInputStream audioInputStream;
	public static void playSound(String sound) {
		try {
		    audioInputStream = AudioSystem.getAudioInputStream(Audio.class.getResourceAsStream("/" +sound +".au"));
		    CurrentClip = AudioSystem.getClip();
		    CurrentClip.open(audioInputStream);

		    CurrentClip.start();
		    } catch(Exception e) {
		    System.err.println("Some Audio Error Ocurred!!");
		    }
		}

	public static void requestPlaySound(String sound) {
		for(int count = 0;count < 10;count++){
			if(sounds[count] == null){
				if(Main.playSounds)
				sounds[count] = sound;
				return;
			}
		}
		
		
		
	}
	
	
	
	public static void update(){
		if(!CurrentClip.isRunning()){
			CurrentClip.close();
			try {
				audioInputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int count = 0;count < 10;count++){
				if(sounds[count] != null){
					playSound(sounds[count]);
					sounds[count] = null;
					return;
					
				}
			}
		}
	}

public static void stopCurrentSound(){
	CurrentClip.stop();
}
public static void playRandomSong(){
	requestPlaySound("Song_" + 1);
}
public static void removeAllSounds(){
	for(int count = 0;count < 10;count++){
		sounds[count] = null;
	}
}


}
