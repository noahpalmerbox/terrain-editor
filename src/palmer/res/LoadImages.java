package palmer.res;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import java.io.IOException;

import javax.imageio.ImageIO;

import palmer.blocks.Block;
import palmer.main.Main;
import palmer.main.Orentation;

public class LoadImages {
	public static final int[] RED = new int[] { 0xFF000000, 0xFF110000,
			0xFF220000, 0xFF333333, 0xFF440000, 0xFF550000, 0xFF550000,
			0xFF660000, 0xFF770000, 0xFF880000, 0xFF990000, 0xFFAA0000,
			0xFFBB0000, 0xFFCC0000, 0xFFDD0000, 0xFFEE0000, 0xFFFF0000 };
	public static final int WHITE = 0xFFFFFF;
	public static final int YELLOW = 0xe9e40e;
	public static final int GREEN = 0x13a200;
	public static final int BLACK = 0;
	public static final int BROWN = 0x6b4a33;
	public static final int[] GREY = new int[] { 0x000000, 0x111111, 0x222222,
			0x333333, 0x444444, 0x444444, 0x555555, 0x666666, 0x777777,
			0x888888, 0x999999, 0xAAAAAA, 0xBBBBBB, 0xCCCCCC, 0xDDDDDD,
			0xEEEEEE, 0xFFFFFF };
	public static volatile int[] Tex_Color = new int[64 * 4];
	public static volatile int[] Item_Color = new int[64 * 4];
	public static volatile int[] Menu_Color = new int[64 * 4];

	public BufferedImage[] getBlocks(String BufferedImage) {
		colorInit();
		try {
			BufferedImage BufferedImages = ImageIO.read(LoadImages.class
					.getResourceAsStream(BufferedImage));
			BufferedImage blocks[] = new BufferedImage[64];
			
			int count = 0;
			for (int y = 0; y < 8; y++) {
				for (int x = 0; x < 8; x++) {
					blocks[count] = BufferedImages.getSubimage(x * 8, y * 8, 8,
							8);

					count++;
				}
			}
			for (int count2 = 0; count2 < 64; count2++) {
				System.out.println("Test");
				int pixels[] = blocks[count2].getRGB(0, 0, 8, 8, null, 0, 8);

				for (int count1 = 0; count1 < 64; count1++) {
					if (pixels[count1] == 0xFF444444)
						pixels[count1] = Tex_Color[count2 * 4 + 0];
					if (pixels[count1] == 0xFF888888)
						pixels[count1] = Tex_Color[count2 * 4 + 1];
					if (pixels[count1] == 0xFFCCCCCC)
						pixels[count1] = Tex_Color[count2 * 4 + 2];
					if (pixels[count1] == 0xFFFFFFFF)
						pixels[count1] = Tex_Color[count2 * 4 + 3];

				}

				blocks[count2].setRGB(0, 0, 8, 8, pixels, 0, 8);

			}
			return blocks;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}
	public BufferedImage[] getGreyTexture(String BufferedImage){
		BufferedImage[] blocks = new BufferedImage[64];
		BufferedImage bi = null;
		try {
			bi = getOutTex(BufferedImage);
		} catch (IOException e){
			e.printStackTrace();
		}
		int count = 0;
		for(int x = 0; x < 8;x++){
			for(int y = 0; y < 8;y++){
				blocks[count] = bi.getSubimage(x*32, y*32, 8, 8);
				blocks[count] = resize(blocks[count], 32, 32);
				count++;
				}	
			}
		return blocks;
	}
	public BufferedImage[] getGreyTexture(BufferedImage bi, int[] a){
		BufferedImage[] blocks = new BufferedImage[64];
		
		
		int count = 0;
		for(int x = 0; x < 8;x++){
			for(int y = 0; y < 8;y++){
				blocks[count] = bi.getSubimage(x*8, y*8, 8, 8);
				blocks[count] = resize(blocks[count], 32, 32);
				count++;
				}	
			}
		return blocks;
	}
	
	public BufferedImage[] getBlocks(BufferedImage bi , int[] a) {
		colorInit();

		BufferedImage blocks[] = new BufferedImage[64];
		int count = 0;
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				blocks[count] = bi.getSubimage(x * 8, y * 8, 8, 8);

				count++;
			}
		}
		for (int count2 = 0; count2 < 64; count2++) {
			int pixels[] = blocks[count2].getRGB(0, 0, 8, 8, null, 0, 8);

			for (int count1 = 0; count1 < 64; count1++) {
				if (pixels[count1] == 0xFF444444)
					pixels[count1] = a[count2 * 4 + 0];
				if (pixels[count1] == 0xFF888888)
					pixels[count1] = a[count2 * 4 + 1];
				if (pixels[count1] == 0xFFCCCCCC)
					pixels[count1] = a[count2 * 4 + 2];
				if (pixels[count1] == 0xFFFFFFFF)
					pixels[count1] = a[count2 * 4 + 3];
			}

			blocks[count2].setRGB(0, 0, 8, 8, pixels, 0, 8);

		}

		return blocks;

	}

	public BufferedImage getBlock(String Block_Name) throws IOException {

		return ImageIO.read(this.getClass().getResourceAsStream(
				"Block_" + Block_Name + ".png"));
	}

	public BufferedImage getTex(String Tex_Name) throws IOException {

		return ImageIO.read(this.getClass().getResourceAsStream(
				Tex_Name + ".png"));
	}

	public BufferedImage getOutTex(String Tex_Name) throws IOException {

		return ImageIO.read(LoadImages.class.getResourceAsStream(Tex_Name
				+ ".png"));
	}

	public BufferedImage getGui(String Gui_Name) throws IOException {
		return ImageIO.read(this.getClass().getResourceAsStream(
				"Gui_" + Gui_Name + ".png"));

	}

	public BufferedImage getEntity(String Entity_Name) throws IOException {
		return ImageIO.read(this.getClass().getResourceAsStream(
				"Entity_" + Entity_Name + ".png"));

	}

	// Not My Code(Taken from a java fourm)
	public BufferedImage resize(BufferedImage source, int width, int height) {
		return commonResize(source, width, height,
				RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
	}

	private static BufferedImage commonResize(BufferedImage source, int width,
			int height, Object hint) {
		BufferedImage img = new BufferedImage(width, height, source.getType());
		Graphics2D g = img.createGraphics();
		try {
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
			g.drawImage(source, 0, 0, width, height, null);
		} finally {
			g.dispose();
		}
		return img;
	}

	// End of Not My Code
	public LoadImages colorInit() {
		
		//***Blocks***
		
		
		//TextColor[( Block id) * 4 + (color #)] = (Hex Color);
		
		// Air
		Tex_Color[0] = 0x96d4f4;
		Tex_Color[1] = 0xa2e0ff;
		Tex_Color[2] = 0xa2e0ff;
		Tex_Color[3] = BLACK;
		// Cloud
		Tex_Color[7] = GREY[16];
		// Stone
		Tex_Color[8] = GREY[4];
		Tex_Color[9] = GREY[8];
		Tex_Color[10] = GREY[12];
		
		//Dirt
		Tex_Color[12] = 0x603f29;
		Tex_Color[13] = 0x825537;
		
		// MenuEmpty
		Tex_Color[16] = BLACK;
		Tex_Color[19] = WHITE;

		//grass
		Tex_Color[20] = 0x603f29;
		Tex_Color[21] = 0x825537;
		Tex_Color[22] = 0x058500;
		Tex_Color[23] = 0x08cb00;
		//Water 1
		Tex_Color[24] = 0x0063ce;
		Tex_Color[25] = 0x0078ff;
		Tex_Color[26] = 0x3b97ff;
		
		
		
		// Bomb
		Tex_Color[28] = 0x96d4f4;
		Tex_Color[29] = BLACK;
		Tex_Color[30] = WHITE;
		Tex_Color[31] = 0xff5a00;
		// logs
		Tex_Color[32] = BLACK;
		Tex_Color[33] = 0x5c3c28;
		Tex_Color[34] = 0x7e5237;
		// Branches
		Tex_Color[36] = 0x105000;
		Tex_Color[37] = 0x1d9100;
		Tex_Color[38] = 0x0a3400;
		// Nuke
		Tex_Color[44] = RED[16];
		Tex_Color[45] = YELLOW;
		Tex_Color[46] = BLACK;
		// Slime
		Tex_Color[48] = 0x02babc;
		Tex_Color[49] = 0x007a7b;
		Tex_Color[50] = BLACK;
		// Reset
		Tex_Color[52] = 0xa3d9f6;
		Tex_Color[53] = 0xa29fa2;
		Tex_Color[54] = 0x82644d;
		Tex_Color[55] = GREEN;
		// Tree
		Tex_Color[56] = 0x0091cb;
		Tex_Color[57] = 0x755338;
		Tex_Color[58] = GREEN;
		//Tex_Color[58] = YELLOW;
		// Bricks
		Tex_Color[60] = GREEN;
		Tex_Color[61] = GREY[15];
		// Insta House
		Tex_Color[20 * 4 + 0] = 0x66462f;
		Tex_Color[20 * 4 + 1] = 0x078c00;
		Tex_Color[20 * 4 + 2] = RED[10];
		Tex_Color[20 * 4 + 3] = 0x96d4f4;
		// Ladder
		Tex_Color[23 * 4 + 0] = 0x755338;
		// WarpPad
		Tex_Color[30 * 4 + 0] = GREEN;
		Tex_Color[30 * 4 + 1] = BLACK;
		Tex_Color[30 * 4 + 2] = GREEN;
		Tex_Color[30 * 4 + 3] = BLACK;
		//Radio
		Tex_Color[31 * 4 + 0] = BLACK;
		Tex_Color[31 * 4 + 1] = BROWN;
		Tex_Color[31 * 4 + 2] = GREY[14];
		Tex_Color[31 * 4 + 3] = BLACK;
		//Chest
		Tex_Color[32*4 + 0] = 0x776818;
		Tex_Color[32*4 + 1] = 0x1e1e1e;
		Tex_Color[32*4 + 2] = 0x838383;
		Tex_Color[32*4 + 3] = 0x473e0e;
		//deadBranches
		Tex_Color[33 * 4 + 0] = 0x473e0e;
		Tex_Color[33 * 4 + 1] = 0x342e0b;
		Tex_Color[33 * 4 + 2] = 0x86751b;
		//sand
		Tex_Color[34 * 4 + 0] = 0xe3dd46;
		Tex_Color[34 * 4 + 1] = 0xbbb63a;
		Tex_Color[34 * 4 + 2] = 0xfcf64e;
		//cactus
		Tex_Color[35 * 4 + 0] = 0x19743d;
		Tex_Color[35 * 4 + 1] = 0x27b861;
		Tex_Color[35 * 4 + 2] = 0x20964f;
		//Water 2
		Tex_Color[36 * 4 + 0] = 0x0063ce;
		Tex_Color[36 * 4 + 1] = 0x0078ff;
		
		
		
		//***items***
		
		//Test
		Item_Color[0 * 4 + 0] = BLACK;
		Item_Color[0 * 4 + 1] = YELLOW;
		//Wand
		Item_Color[1*4 + 0] = 0x3f2e00;
		Item_Color[1*4 + 1] = 0x8a6500;
		Item_Color[1*4 + 2] = 0xcccccc;
		
		//***Menu***
		
		//play
		Menu_Color[0] = BLACK;
		Menu_Color[1] = GREEN;
		Menu_Color[4] = BLACK;
		Menu_Color[5] = GREY[4];
		Menu_Color[8] = BLACK;
		Menu_Color[9] = GREEN;
		Menu_Color[12] = BLACK;
		Menu_Color[13] = GREEN;
		Menu_Color[16] = BLACK;
		Menu_Color[17] = RED[14];
		Menu_Color[20] = 0x0063ce;
		Menu_Color[21] = 0x0078ff;
		Menu_Color[22] = 0x3b97ff;
		Menu_Color[23] = RED[14];
		Menu_Color[24] = 0xff8102;
		Menu_Color[25] = RED[14];
		Menu_Color[7*4 + 0] = BLACK;
		Menu_Color[7*4 + 1] = WHITE;
		Menu_Color[8*4 + 0] = BLACK;
		Menu_Color[8*4 + 1] = WHITE;
		Menu_Color[9*4 + 0] = BLACK;
		Menu_Color[9*4 + 1] = WHITE;
		Menu_Color[10*4 + 0] = BLACK;
		Menu_Color[10*4 + 1] = WHITE;
		Menu_Color[11*4 + 0] = BLACK;
		Menu_Color[11*4 + 1] = WHITE;
		Menu_Color[12*4 + 0] = BLACK;
		Menu_Color[12*4 + 1] = WHITE;
		Menu_Color[13*4 + 0] = BLACK;
		Menu_Color[13*4 + 1] = WHITE;
		Menu_Color[14*4 + 0] = BLACK;
		Menu_Color[14*4 + 1] = WHITE;
		Menu_Color[15*4 + 0] = BLACK;
		Menu_Color[15*4 + 1] = WHITE;
		Menu_Color[16*4 + 0] = BLACK;
		Menu_Color[16*4 + 1] = WHITE;
		Menu_Color[17*4 + 0] = BLACK;
		Menu_Color[17*4 + 1] = WHITE;
		return this;

	}

	public BufferedImage combine(CombineType ct) {
		BufferedImage a;
		BufferedImage b;
		BufferedImage c = new BufferedImage(32, 32, BufferedImage.TYPE_INT_RGB);
		switch (ct) {
		case BrickStairsR:
			a = Main.Textures[0];
			b = Main.Textures[15];
			Graphics g = c.getGraphics();
			g.drawImage(b, 0, 0, null);
			g.drawImage(a.getSubimage(16, 0, 16, 16), 16, 0, null);
			g.dispose();
			Main.Textures[18] = c;
			break;
		case BrickStairsL:
			a = Main.Textures[0];
			b = Main.Textures[15];
			Graphics g2 = c.getGraphics();
			g2.drawImage(b, 0, 0, null);
			g2.drawImage(a.getSubimage(0, 0, 16, 16), 0, 0, null);
			g2.dispose();
			Main.Textures[19] = c;
			break;
		case BricksInside:
			a = Main.Textures[15];
			Graphics g3 = c.getGraphics();
			g3.drawImage(a, 0, 0, null);
			try {
				g3.drawImage(getTex("MenuBack"), 0, 0, null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			g3.dispose();
			Main.Textures[21] = c;
			break;
		case BrickDoorL:
			a = Main.Textures[21];
			Graphics g4 = c.getGraphics();
			g4.drawImage(a, 0, 0, null);
			g4.setColor(Color.DARK_GRAY);
			g4.fillRect(0, 0, 8, 32);
			g4.dispose();
			Main.Textures[22] = c;
			break;
		case BrickDoorR:
			a = Main.Textures[21];
			Graphics g5 = c.getGraphics();
			g5.drawImage(a, 0, 0, null);
			g5.setColor(Color.DARK_GRAY);
			g5.fillRect(24, 0, 8, 32);
			g5.dispose();
			Main.Textures[24] = c;
			break;
		case Ladder:
			a = Main.Textures[21];
			b = Main.Textures[23];
			int apixels[] = a.getRGB(0, 0, 32, 32, null, 0, 32);
			int bpixels[] = b.getRGB(0, 0, 32, 32, null, 0, 32);
			Graphics g6 = c.getGraphics();
			for (int count = 0; count < 1024; count++) {
				if (bpixels[count] == 0xFFFFFFFF) {
					bpixels[count] = apixels[count];
				}
				c.setRGB(0, 0, 32, 32, bpixels, 0, 32);
			}

			g6.dispose();
			Main.Textures[23] = c;
			break;
		case Lava:
			Graphics g7 = c.getGraphics();
			g7.setColor(new Color(0xff, 0x81, 0x02));
			g7.fillRect(0, 0, 32, 32);
			g7.dispose();
			Main.Textures[25] = c;

			break;
		case StoneInside:
			a = Main.Textures[2];
			Graphics g8 = c.getGraphics();
			g8.drawImage(a, 0, 0, null);
			try {
				g8.drawImage(getTex("MenuBack"), 0, 0, null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			g8.dispose();
			Main.Textures[26] = c;
			break;
		case IronInside:
			a = Main.Textures[26];
			Graphics g9 = c.getGraphics();
			g9.drawImage(a, 0, 0, null);
			g9.setColor(new Color(0xe3, 0xc0, 0xaa));
			int cpixels[] = new int[1024];
			int qpixels[] = Main.Textures[26].getRGB(0, 0, 32, 32, null, 0, 32);
			int pixels[] = Main.Textures[2].getRGB(0, 0, 32, 32, null, 0, 32);
			for (int count = 0; count < 1024; count++) {
				if (pixels[count] == 0xFF444444) {
					cpixels[count] = 0xe3c0aa;

				} else {
					cpixels[count] = qpixels[count];
				}
			}
			c.setRGB(0, 0, 32, 32, cpixels, 0, 32);
			Main.Textures[27] = c;
			break;
		case GoldInside:
			a = Main.Textures[26];
			Graphics g10 = c.getGraphics();
			g10.drawImage(a, 0, 0, null);
			g10.setColor(new Color(0xe3, 0xc0, 0xaa));
			int c2pixels[] = new int[1024];
			int q2pixels[] = Main.Textures[26]
					.getRGB(0, 0, 32, 32, null, 0, 32);
			int p2ixels[] = Main.Textures[2].getRGB(0, 0, 32, 32, null, 0, 32);
			for (int count = 0; count < 1024; count++) {
				if (p2ixels[count] == 0xFF444444) {
					c2pixels[count] = 0xe6d019;

				} else {
					c2pixels[count] = q2pixels[count];
				}
			}
			c.setRGB(0, 0, 32, 32, c2pixels, 0, 32);
			Main.Textures[28] = c;
			break;
		case DiamondInside:
			a = Main.Textures[26];
			Graphics g11 = c.getGraphics();
			g11.drawImage(a, 0, 0, null);
			g11.setColor(new Color(0xe3, 0xc0, 0xaa));
			int c3pixels[] = new int[1024];
			int q3pixels[] = Main.Textures[26]
					.getRGB(0, 0, 32, 32, null, 0, 32);
			int p3ixels[] = Main.Textures[2].getRGB(0, 0, 32, 32, null, 0, 32);
			for (int count = 0; count < 1024; count++) {
				if (p3ixels[count] == 0xFF444444) {
					c3pixels[count] = 0x19e6dc;

				} else {
					c3pixels[count] = q3pixels[count];
				}
			}
			c.setRGB(0, 0, 32, 32, c3pixels, 0, 32);
			Main.Textures[29] = c;
			break;
	
		
		}

		return null;

	}

	public BufferedImage screen(Block[][] a) {
		BufferedImage c = new BufferedImage(16 * 32, 16 * 32,
				BufferedImage.TYPE_INT_RGB);
		Graphics g = c.getGraphics();
		for (int x = 0; x < 17; x++) {
			for (int y = 0; y < 17; y++) {
				if (Main.rloop.lv1.getBlockGrid(x, y) != null)
					g.drawImage(a[x][y].Texture,
							x * 32, y * 32, null);
			}
		}
		return c;

	}

	public BufferedImage screen(BufferedImage a, BufferedImage b, Orentation o) {
		
		BufferedImage c = null;
		if (o == Orentation.Up || o == Orentation.Down) {
			c = new BufferedImage(16 * 32, 32 * 32, BufferedImage.TYPE_INT_RGB);
		}
		if (o == Orentation.Right || o == Orentation.Left) {
			c = new BufferedImage(32 * 32, 16 * 32, BufferedImage.TYPE_INT_RGB);
		}
		
		
		
		Graphics g = c.getGraphics();
		switch (o) {
		case Up:
			g.drawImage(a, 0, 0, null);
			g.drawImage(b, 0, 16 * 32, null);
			break;
		case Down:
			g.drawImage(a, 0, 0, null);
			g.drawImage(b, 0, 16 * 32, null);

			break;
		case Left:
			g.drawImage(b, 0, 0, null);
			g.drawImage(a, 16 * 32, 0, null);
			break;
		case Right:
			g.drawImage(a, 0, 0, null);
			g.drawImage(b, 16 * 32, 0, null);
			break;

		}
		return c;

	}

	public BufferedImage fixStairs() {
		BufferedImage c = new BufferedImage(32, 32, BufferedImage.TYPE_INT_RGB);
		Graphics g = c.getGraphics();
		g.drawImage(Main.Textures[15], 0, 0, null);
		g.drawImage(Main.rloop.lv1.cust_Air.Texture.getSubimage(0, 0, 16, 16),
				0, 0, null);
		Block.brickStairsL.Texture = c;
		BufferedImage d = new BufferedImage(32, 32, BufferedImage.TYPE_INT_RGB);
		Graphics g2 = d.getGraphics();
		g2.drawImage(Main.Textures[15], 0, 0, null);
		g2.drawImage(
				Main.rloop.lv1.cust_Air.Texture.getSubimage(16, 0, 16, 16), 16,
				0, null);
		Block.brickStairsR.Texture = d;
		return null;

	}
	
	
	public BufferedImage intParser(int input){
		String s = Integer.toString(input);
		BufferedImage bi = new BufferedImage(s.length()*32,32, BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.getGraphics();
		for(int i = 0;i < s.length();i++){
			g.drawImage(charparse(s.charAt(i)), i*32, 0, null);
		}
	
	return bi;
	}
private BufferedImage charparse(char ch){
	switch(ch){
	case '0': return Main.MenuTextures[16];
	case '1': return Main.MenuTextures[7];
	case '2': return Main.MenuTextures[8];
	case '3': return Main.MenuTextures[9];
	case '4': return Main.MenuTextures[10];
	case '5': return Main.MenuTextures[11];
	case '6': return Main.MenuTextures[12];
	case '7': return Main.MenuTextures[13];
	case '8': return Main.MenuTextures[14];
	case '9': return Main.MenuTextures[15];
	case '-': return Main.MenuTextures[17];

		}
return Main.MenuTextures[4];	

}



}
