	package palmer.main;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import palmer.blocks.Block;
import palmer.entity.TileEntity;
import palmer.entity.TileType;
import palmer.entity.ToolChest;
import palmer.gui.PauseMenu;
import palmer.levels.GameRules;
import palmer.levels.Level;
import palmer.levels.LevelGen;
import palmer.res.LoadImages;

public class Render extends Canvas {
	/**
	 * 
	 */
	public static int TEXTURE_SIZE_X = 32;
	public static int TEXTURE_SIZE_Y = 32;
public int lmX, lmY;
	private static final long serialVersionUID = 1L;
	JPanel panel = new JPanel();
	public GameRules lv1 = LevelGen.getNewLevel();
	public JFrame frame;
	public BufferedImage images[] = new BufferedImage[10];
	public int ix[] = new int[10];
	public int iy[] = new int[10];
	public int iex[] = new int[10];
	public int iey[] = new int[10];
	public Orentation[] o = new Orentation[10];
	public int Oldw = 512, Oldh = 512;
	public TileEntity tiles[] = new TileEntity[10];
	public String con = "";
	public InputHandler in;
	public boolean antiLag = false;
	public int shouldRender = 0;
	public boolean renderpause = false;
	PauseMenu pm = new PauseMenu();
	public LoadImages il = new LoadImages();
	BufferedImage chat;
	public BufferedImage menuBack;
	public int watercount;
	public int oldx, oldy;

	;

	public Render() {
		
		tiles[0] = TileEntity.WarpPad;
		tiles[0].bind(Block.warpPad);
		tiles[1] = TileEntity.Radio;
		tiles[1].bind(Block.radio);
		tiles[2] = new ToolChest(TileType.ToolChest);
		tiles[2].bind(Block.toolChest);
		in = new InputHandler(this);
		

		try {
			chat = il.getTex("ChatBar");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frame = new JFrame();

		setMinimumSize(new Dimension(512, 512));
		setMaximumSize(new Dimension(1024, 1024));
		setPreferredSize(new Dimension(512, 512));
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.addWindowListener(new Window());
		frame.add(this, BorderLayout.CENTER);
		frame.pack();
		setFocusable(true);
		requestFocus();
		frame.setVisible(true);
		
		try {
			menuBack = Main.li.getTex("MenuBack");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

	}

	public void Rendering() {
		BufferStrategy bf = Main.rloop.getBufferStrategy();
		if (bf == null) {
			createBufferStrategy(3);
			return;
		}

		Graphics g = null;
		g = bf.getDrawGraphics();

		lv1.colision();
		
		lv1.update();
		update();
		shouldRender++;
		
		draw(g);
		lv1.menDraw(g);
		
		bf.show();
		Toolkit.getDefaultToolkit().sync();

	}

	private void update() {
		for (int count = 0; count < 10; count++) {
			if (tiles[count] != null)
				tiles[count].update();
		}
	}

	public void draw(Graphics g) {
		
		if (Main.clear || lv1.menu) {
			for (int x = 0; x < 16; x++) {
				for (int y = 0; y < 16; y++) {

					if (lv1.getBlockGrid(x, y) == null) {
						lv1.changeBlock(x, y, Block.air);
					}

					g.drawImage(lv1.getBlockGrid(x, y).Texture, (x * TEXTURE_SIZE_X),
							((y * TEXTURE_SIZE_Y)), TEXTURE_SIZE_X, TEXTURE_SIZE_Y, Main.rloop);
				}
			}
			for (int e = 0; e < 20; e++) {
				if (lv1.getEntity(e) != null)
					g.drawImage(lv1.getEntityTexture(e), lv1.getEntityX(e),
							lv1.getEntityY(e), Main.rloop);
			}
			g.drawImage(lv1.player.Texture, lv1.player.x + 1, lv1.player.y,
					TEXTURE_SIZE_X,TEXTURE_SIZE_Y,Main.rloop);
			g.drawImage(chat, 0, 375, (con.length() + 16) * 6, 16, Main.rloop);
			g.setColor(Color.WHITE);
			g.drawString("[Terrain Editor]" + con, 10, 387);

			Main.clear = false;
		} else {
			for (int x = 0; x < 16; x++) {
				for (int y = 0; y < 16; y++) {

					if (lv1.getBlockGrid(x, y) == null) {
						lv1.changeBlock(x, y, Block.air);
					}
					if (antiLag) {
						frame.resize(512,  512);
						if ((lv1.bs.x / 32 + 5 > x && lv1.bs.x / 32 - 5 < x)
								&& (lv1.bs.y / 32 + 5 > y && lv1.bs.y / 32 - 5 < y)
								|| lv1.player.x / 32 - 5 < x
								&& lv1.player.x / 32 + 5 > x)
							g.drawImage(lv1.getBlockGrid(x, y).Texture,
									(x * 32), ((y * 32)), Main.rloop);
					} else {
						g.drawImage(lv1.getBlockGrid(x, y).Texture, (x * TEXTURE_SIZE_X),
								((y * TEXTURE_SIZE_Y)),TEXTURE_SIZE_X,TEXTURE_SIZE_Y, Main.rloop);
					}

					// Drawing the player

					// drawing the block selector

				}// end of inner for-loop
			}// end of outer for-loop

			for (int e = 0; e < 20; e++) {
				if (lv1.getEntity(e) != null)
					g.drawImage(lv1.getEntityTexture(e), lv1.getEntityX(e),
							lv1.getEntityY(e), Main.rloop);
			}
			g.drawImage(lv1.player.Texture, lv1.player.x + 1, lv1.player.y,TEXTURE_SIZE_X,TEXTURE_SIZE_Y,
					Main.rloop);
			g.drawImage(lv1.bs.Texture, lv1.bs.x - 5, lv1.bs.y - 5,TEXTURE_SIZE_X,TEXTURE_SIZE_Y, Main.rloop);

		}

		for (int count1 = 0; count1 < 10; count1++) {
			if (images[count1] != null) {
				if (o[count1] == Orentation.Up)
					g.drawImage(images[count1], ix[count1], iy[count1] -= 4,
							null);

				if (iy[count1] < (16*32)) {
					removeImage(count1);
				}
				if(o[count1] == Orentation.Down){
					System.out.println("Pie");
					g.drawImage(images[count1], ix[count1], iy[count1] += 4,
							null);

				if (iy[count1] > -(16*32)) {
					removeImage(count1);
				}
					
					
				}
			
			
			
			
			}
			
		}
		g.drawImage(chat, 0, 375, (con.length() + 16) * 6, 16, Main.rloop);
		g.setColor(Color.WHITE);
		typeRead();
		g.drawString("[Terrain Editor]" + con, 10, 387);
		g.drawString(("Water: " + watercount), 20, 20);

		for (int count = 0; count < 10; count++) {
			if (tiles[count] != null) {
				if (tiles[count].active) {
					tiles[count].Render(g);
				}
			}
		}
		if(lv1.player.i.shouldRender)
		lv1.player.i.render(g);
		ToolChest misc = (ToolChest) Block.toolChest.entity;
		if(misc.i.shouldRender){
			misc.i.render(g);
		}
		if(renderpause){
			pm.drawPauseMenu(g);
		}
		if(frame.getWidth() != getWidth()){
			resizeScreen(frame.getWidth()/16, frame.getHeight()/17);
		
		}
		
		
		

	}

	public void addImage(BufferedImage b, int startX, int startY, int endX,
			int endY) {
		for (int count = 0; count < 10; count++) {
			if (images[count] == null) {
				images[count] = b;
				ix[count] = startX;
				iy[count] = startY;
				
				return;
			}
		}

	}

	public void conType() {
		Main.in.a.enable();
		Main.in.b.enable();
		Main.in.c.enable();
		Main.in.d.enable();
		Main.in.e.enable();
		Main.in.f.enable();
		Main.in.g.enable();
		Main.in.h.enable();
		Main.in.i.enable();
		Main.in.j.enable();
		Main.in.k.enable();
		Main.in.l.enable();
		Main.in.m.enable();
		Main.in.n.enable();
		Main.in.o.enable();
		Main.in.p.enable();
		Main.in.q.enable();
		Main.in.r.enable();
		Main.in.s.enable();
		Main.in.t.enable();
		Main.in.u.enable();
		Main.in.v.enable();
		Main.in.w.enable();
		Main.in.x.enable();
		Main.in.y.enable();
		Main.in.z.enable();
	}

	public void unConType() {
		Main.in.a.disable();
		Main.in.b.disable();
		Main.in.c.disable();
		Main.in.d.disable();
		Main.in.e.disable();
		Main.in.f.disable();
		Main.in.g.disable();
		Main.in.h.disable();
		Main.in.i.disable();
		Main.in.j.disable();
		Main.in.k.disable();
		Main.in.l.disable();
		Main.in.m.disable();
		Main.in.n.disable();
		Main.in.o.disable();
		Main.in.p.disable();
		Main.in.q.disable();
		Main.in.r.disable();
		Main.in.s.disable();
		Main.in.t.disable();
		Main.in.u.disable();
		Main.in.v.disable();
		Main.in.w.disable();
		Main.in.x.disable();
		Main.in.y.disable();
		Main.in.z.disable();
	}

	public void typeRead() {
		if (Main.in == null)
			return;
		if (Main.in.a.isEnabled()) {
			if (Main.in.a.typed) {
				con = (con + "a");
				Main.in.a.typed = false;
			}
			if (Main.in.b.typed) {
				con = (con + "b");
				Main.in.b.typed = false;
			}
			if (Main.in.c.typed) {
				con = (con + "c");
				Main.in.c.typed = false;
			}
			if (Main.in.d.typed) {
				con = (con + "d");
				Main.in.d.typed = false;
			}
			if (Main.in.e.typed) {
				con = (con + "e");
				Main.in.e.typed = false;
			}
			if (Main.in.f.typed) {
				con = (con + "f");
				Main.in.f.typed = false;
			}
			if (Main.in.g.typed) {
				con = (con + "g");
				Main.in.g.typed = false;
			}
			if (Main.in.h.typed) {
				con = (con + "h");
				Main.in.h.typed = false;
			}
			if (Main.in.i.typed) {
				con = (con + "i");
				Main.in.i.typed = false;
			}
			if (Main.in.j.typed) {
				con = (con + "j");
				Main.in.j.typed = false;
			}
			if (Main.in.k.typed) {
				con = (con + "k");
				Main.in.k.typed = false;
			}
			if (Main.in.l.typed) {
				con = (con + "l");
				Main.in.l.typed = false;
			}
			if (Main.in.m.typed) {
				con = (con + "m");
				Main.in.m.typed = false;
			}
			if (Main.in.n.typed) {
				con = (con + "c");
				Main.in.n.typed = false;
			}
			if (Main.in.o.typed) {
				con = (con + "o");
				Main.in.o.typed = false;
			}
			if (Main.in.p.typed) {
				con = (con + "p");
				Main.in.p.typed = false;
			}
			if (Main.in.q.typed) {
				con = (con + "q");
				Main.in.q.typed = false;
			}
			if (Main.in.r.typed) {
				con = (con + "r");
				Main.in.r.typed = false;
			}
			if (Main.in.s.typed) {
				con = (con + "s");
				Main.in.s.typed = false;
			}
			if (Main.in.t.typed) {
				con = (con + "t");
				Main.in.t.typed = false;
			}
			if (Main.in.u.typed) {
				con = (con + "u");
				Main.in.u.typed = false;
			}
			if (Main.in.v.typed) {
				con = (con + "v");
				Main.in.v.typed = false;
			}
			if (Main.in.w.typed) {
				con = (con + "w");
				Main.in.w.typed = false;
			}
			if (Main.in.x.typed) {
				con = (con + "x");
				Main.in.x.typed = false;
			}
			if (Main.in.y.typed) {
				con = (con + "y");
				Main.in.y.typed = false;
			}
			if (Main.in.z.typed) {
				con = (con + "z");
				Main.in.z.typed = false;
			}

		}
	}

	public void removeImage(int array) {
		images[array] = null;

	}



	public void resizeScreen(int w, int h){
	
	TEXTURE_SIZE_X = w;
	TEXTURE_SIZE_Y = h;
	
	
	
	
	
	
	
	}


}
