package palmer.main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Random;

import palmer.blocks.Block;
import palmer.entity.ToolChest;
import palmer.gui.*;
import palmer.exceptions.IncorrectItemException;
import palmer.exceptions.KeyNotEnabledException;
import palmer.tools.Tool;

public class InputHandler implements KeyListener, MouseListener,
		MouseMotionListener {
	Render ren;
	public boolean mouseSkip = false;
	public InputHandler(Render red) {
		red.addKeyListener(this);
		red.addMouseListener(this);
		red.addMouseMotionListener(this);
		ren = red;
	}

	public class Key {
		public Key(int i) {
			code = i;
		}

		public Key(int i, boolean b) {
			code = i;
			enabled = true;
		}

		private boolean enabled = true;
		private int code;
		public boolean typed = false;
		private boolean isPressed;

		public boolean getIsPressed() {

			if (enabled)
				return isPressed;
			else {
				return false;
			}

		}

		public void toggleIsPressed(boolean b) {
			isPressed = b;
		}

		public int getCode() {
			return code;
		}

		public void enable() {
			enabled = true;
		}

		public void disable() {
			enabled = false;
		}

		public boolean isEnabled() {
			return enabled;
		}
	}

	// Motion keys
	public Key up = new Key(KeyEvent.VK_W);
	public Key down = new Key(KeyEvent.VK_S);
	public Key left = new Key(KeyEvent.VK_A);
	public Key right = new Key(KeyEvent.VK_D);
	// public Key inventory = new Key(KeyEvent.VK_I);
	// Typing keys
	public Key a = new Key(KeyEvent.VK_A, false);
	public Key b = new Key(KeyEvent.VK_B, false);
	public Key c = new Key(KeyEvent.VK_C, false);
	public Key d = new Key(KeyEvent.VK_D, false);
	public Key e = new Key(KeyEvent.VK_E, false);
	public Key f = new Key(KeyEvent.VK_F, false);
	public Key g = new Key(KeyEvent.VK_G, false);
	public Key h = new Key(KeyEvent.VK_H, false);
	public Key i = new Key(KeyEvent.VK_I, false);
	public Key j = new Key(KeyEvent.VK_J, false);
	public Key k = new Key(KeyEvent.VK_K, false);
	public Key l = new Key(KeyEvent.VK_L, false);
	public Key m = new Key(KeyEvent.VK_M, false);
	public Key n = new Key(KeyEvent.VK_N, false);
	public Key o = new Key(KeyEvent.VK_O, false);
	public Key p = new Key(KeyEvent.VK_P, false);
	public Key q = new Key(KeyEvent.VK_Q, false);
	public Key r = new Key(KeyEvent.VK_R, false);
	public Key s = new Key(KeyEvent.VK_S, false);
	public Key t = new Key(KeyEvent.VK_T, false);
	public Key u = new Key(KeyEvent.VK_U, false);
	public Key v = new Key(KeyEvent.VK_V, false);
	public Key w = new Key(KeyEvent.VK_W, false);
	public Key x = new Key(KeyEvent.VK_X, false);
	public Key y = new Key(KeyEvent.VK_Y, false);
	public Key z = new Key(KeyEvent.VK_Z, false);

	@Override
	public void mouseDragged(MouseEvent e) {

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		int x = e.getX() / ren.TEXTURE_SIZE_X;
		int y = e.getY() / ren.TEXTURE_SIZE_Y;
		ren.lv1.setSelector(x, y);

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(ren.renderpause){
			
			ren.pm.handleClick(e);
		}
		if(mouseSkip){
			mouseSkip = false;
			return;
		}
		if (!ren.lv1.menu) {
			if (Main.Mapchanges) {
				if (e.getButton() == 1) {
					if(ren.lv1.selt != null){
						ren.lv1.selt.use(ren.lv1.getBlockGrid(ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y));
						return;
					}
					if (ren.lv1.selb.id == 18 || ren.lv1.selb.id == 19) {
						Main.li.fixStairs();
					}
					if(Main.gm == GameMode.s){
					if(ren.lv1.player.i.hasItem(ren.lv1.selb) != -1){
					ren.lv1.changeBlock(ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y,
							ren.lv1.selb);
					try {
						ren.lv1.player.i.getItem(ren.lv1.player.i.hasItem(ren.lv1.selb));
					} catch (IncorrectItemException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					}else{
						ren.con = "Whoops you're out of that type of block";
					}
					}
				if(Main.gm == GameMode.c){
					ren.lv1.changeBlock(ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y,
							ren.lv1.selb);
				}

				}
				if (e.getButton() == 3) {
					if(new Random().nextBoolean()){
						if(ren.lv1.getBlockGrid(
								ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y).id == 9 || ren.lv1.getBlockGrid(
										ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y).id == 37 ){
							ren.lv1.player.i.addItem(Block.tree);
							
						}
					}else if(ren.lv1.getBlockGrid(
							ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y).id != 9 || ren.lv1.getBlockGrid(
									ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y).id != 37){
					ren.lv1.player.i.addItem(ren.lv1.getBlockGrid(
							ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y));
					}
					ren.lv1.changeBlock(ren.lv1.bs.x / ren.TEXTURE_SIZE_X, ren.lv1.bs.y / ren.TEXTURE_SIZE_Y,
							ren.lv1.cust_Air);
				}
			}
		} else {
			if (e.getButton() == 1) {
				int x = e.getX() / ren.TEXTURE_SIZE_X - 4;
				int y = ((e.getY() + (ren.TEXTURE_SIZE_Y*(15/16))) / ren.TEXTURE_SIZE_Y) - ren.TEXTURE_SIZE_Y *(1/8);
				//if (x < 0 || y < 0)
					//return;
				//if (x > 5 || y > 5)
					//return;
				ren.lv1.updateSelector(x, y);

				// Main.con.print("x:" + x + " y: " + y);

			}
		}

		if (Main.rloop.tiles[0].active) {
			Main.rloop.tiles[0].HandleClick(e);
		}
		if(Main.rloop.lv1.player.i.shouldRender)
			Main.rloop.lv1.player.i.handleClick(e);
		ToolChest misc = (ToolChest) Block.toolChest.entity;
		if(misc.i.shouldRender){
			misc.i.handleClick(e);
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		KeyEvent(e, true, false);
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			if(ren.renderpause){
				ren.renderpause = false;
				ren.pm.menu = 0;
				GameEvents.fire("allowMapChanges");
			
			}else{
				ren.renderpause = true;
				GameEvents.fire("banMapChanges");
			}	
		}

		if (e.getKeyCode() == KeyEvent.VK_I) {
			if (ren.lv1.menu) {
				ren.lv1.menu = false;
			} else {
				ren.lv1.menu = true;
			}

		}
		if (e.getKeyCode() == KeyEvent.VK_E) {
			if (ren.lv1.player.i.shouldRender) {
				ren.lv1.player.i.shouldRender = false;
				GameEvents.fire("allowMapChanges");
			} else {
				ren.lv1.player.i.shouldRender = true;
				GameEvents.fire("banMapChanges");
			}
		}

		/*
		 * if (e.getKeyCode() == KeyEvent.VK_W) { if (ren.lv1.player.tempy !=
		 * 64) ren.lv1.player.jump = true; } if (e.getKeyCode() ==
		 * KeyEvent.VK_A) { ren.lv1.lgo = true; } if (e.getKeyCode() ==
		 * KeyEvent.VK_S) {
		 * 
		 * } if (e.getKeyCode() == KeyEvent.VK_D) { ren.lv1.rgo = true;
		 * 
		 * } if (e.getKeyCode() == KeyEvent.VK_R) { if
		 * (ren.lv1.version.equals("1")) { ren.lv1.version = "2";
		 * ren.lv1.textureInit(); } else { ren.lv1.version = "1";
		 * ren.lv1.textureInit(); } }
		 */

	}

	@Override
	public void keyReleased(KeyEvent e) {
		KeyEvent(e, false, false);
		/*
		 * if (e.getKeyCode() == KeyEvent.VK_W) { ren.lv1.player.jump = false;
		 * 
		 * }
		 * 
		 * if (e.getKeyCode() == KeyEvent.VK_A) { ren.lv1.lgo = false; } if
		 * (e.getKeyCode() == KeyEvent.VK_S) {
		 * 
		 * } if (e.getKeyCode() == KeyEvent.VK_D) { ren.lv1.rgo = false;
		 * 
		 * }
		 */
	}

	@Override
	public void keyTyped(KeyEvent e) {
		KeyEvent(e, false, true);

	}

	public void KeyEvent(KeyEvent e, boolean b, boolean typed) {
		if (up.getCode() == e.getKeyCode())
			up.toggleIsPressed(b);
		if (down.getCode() == e.getKeyCode())
			down.toggleIsPressed(b);
		if (left.getCode() == e.getKeyCode())
			left.toggleIsPressed(b);
		if (right.getCode() == e.getKeyCode())
			right.toggleIsPressed(b);
		// if(inventory.getCode() ==
		// e.getKeyCode())inventory.toggleIsPressed(b);
		if (typed) {
			if (a.getCode() == e.getKeyCode()) {
				a.typed = true;
			}
			if (this.b.getCode() == e.getKeyCode()) {
				this.b.typed = true;
			}
			if (c.getCode() == e.getKeyCode()) {
				c.typed = true;
			}
			if (d.getCode() == e.getKeyCode()) {
				d.typed = true;
			}
			if (this.e.getCode() == e.getKeyCode()) {
				this.e.typed = true;
			}
			if (f.getCode() == e.getKeyCode()) {
				f.typed = true;
			}
			if (g.getCode() == e.getKeyCode()) {
				g.typed = true;
			}
			if (h.getCode() == e.getKeyCode()) {
				h.typed = true;
			}
			if (i.getCode() == e.getKeyCode()) {
				i.typed = true;
			}
			if (j.getCode() == e.getKeyCode()) {
				j.typed = true;
			}
			if (k.getCode() == e.getKeyCode()) {
				k.typed = true;
			}
			if (l.getCode() == e.getKeyCode()) {
				l.typed = true;
			}
			if (m.getCode() == e.getKeyCode()) {
				m.typed = true;
			}
			if (n.getCode() == e.getKeyCode()) {
				n.typed = true;
			}
			if (o.getCode() == e.getKeyCode()) {
				o.typed = true;
			}
			if (p.getCode() == e.getKeyCode()) {
				p.typed = true;
			}
			if (q.getCode() == e.getKeyCode()) {
				q.typed = true;
			}
			if (r.getCode() == e.getKeyCode()) {
				r.typed = true;
			}
			if (s.getCode() == e.getKeyCode()) {
				s.typed = true;
			}
			if (t.getCode() == e.getKeyCode()) {
				t.typed = true;
			}
			if (u.getCode() == e.getKeyCode()) {
				u.typed = true;
			}
			if (v.getCode() == e.getKeyCode()) {
				v.typed = true;
			}
			if (w.getCode() == e.getKeyCode()) {
				w.typed = true;
			}
			if (x.getCode() == e.getKeyCode()) {
				x.typed = true;
			}
			if (y.getCode() == e.getKeyCode()) {
				y.typed = true;
			}
			if (z.getCode() == e.getKeyCode()) {
				z.typed = true;
			}
		}

	}

}