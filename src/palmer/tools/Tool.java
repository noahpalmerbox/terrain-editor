package palmer.tools;

import java.awt.image.BufferedImage;
import java.io.IOException;

import palmer.blocks.Block;
import palmer.entity.TileType;
import palmer.entity.ToolChest;
import palmer.items.Item;
import palmer.items.ItemType;
import palmer.main.Main;

public abstract class Tool extends Item {
	public static Tool Wand = new Tool(Main.ItemTextures[1],41, ItemType.Tool){
		
		public void use(Block b){
			if(b.hasEntity()){
				if(b.entity.type == TileType.ToolChest){
					ToolChest misc = (ToolChest) b.entity;
					if(!misc.i.shouldRender){
					misc.i.shouldRender = true;
					}else{
						misc.i.shouldRender = false;
					}
					
					}
			}
		}
	};
	public abstract void use(Block b);
	public ToolType tooltype;
	public Tool(BufferedImage b, int id){
		super(id);
		Texture = b;
		
	}
	public Tool(BufferedImage b, int id, ItemType i){
		super(id);
		Texture = b;
		type = i;
		
	}
	public Tool(int id){
		super(id);
		
	}
	public Tool(int id, ItemType i){
		super(id);
		type = i;
		
	}
	public Tool setType(ItemType i){
		type = i;
		return this;
	}

	
	

}
