package palmer.gui;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import palmer.main.GameEvents;
import palmer.main.GameSettings;
import palmer.main.Main;

public class PauseMenu {
	public Menu[] menus = new Menu[10];

	public static enum RenderType {
		Menu, Settings, VideoSettings, WaterSettings, GameEvents
	}

	public int menu = 0;
	// public RenderType rt = RenderType.Menu;
	BlockSelector watersettingsbs = new BlockSelector();
		public int waterbutton;
	@SuppressWarnings("unused")
	public void init() {
		watersettingsbs.x = 96;
		watersettingsbs.y = 1;
		Main: {
			// Main Menu
			menus[0] = Menu.createMenu(new MenuAction[10],new BufferedImage[10], new String[10]);
			// Resume
			menus[0].addItem(new MenuAction(){public void run(){Main.rloop.renderpause = false;GameEvents.fire("allowMapChanges");}}, Main.MenuTextures[0], "Resume");
			//Settings
			menus[0].addItem(new MenuAction(){public void run(){menu = 1;}}, Main.MenuTextures[1], "Settings");
		}
		Settings:{
			menus[1] = Menu.createMenu(new MenuAction[10], new BufferedImage[10], new String[10]);
			//Video Settings
			menus[1].addItem(new MenuAction(){public void run(){menu = 2;}}, Main.Textures[5], "Video Settings");
			//Back
			menus[1].addItem(new MenuAction(){public void run(){menu = 0;}},Main.MenuTextures[2], "Back");
			
		}
		VideoSettings:{
			menus[2] = Menu.createMenu(new MenuAction[10], new BufferedImage[10], new String[10]);
			//Water Settings
			menus[2].addItem(new MenuAction(){public void run(){menu = 3;}}, Main.Textures[6], "Water Settings");
			//Back
			menus[2].addItem(new MenuAction(){public void run(){menu = 1;}}, Main.MenuTextures[2], "Back");
		}
		WaterSettings:{
			menus[3] = Menu.createMenu(new MenuAction[10], new BufferedImage[10], new String[10]);
			//Enable/Disable HQ Water
			waterbutton = menus[3].addItem(new MenuAction(){public void run(){if(GameSettings.HQWater)GameSettings.HQWater = false;else GameSettings.HQWater = true;}},Main.MenuTextures[4] ,"Enable HQ Water" );
			//Back
			menus[3].addItem(new MenuAction(){public void run(){menu = 2;}}, Main.MenuTextures[2], "Back");
		}
	}

	public void drawPauseMenu(Graphics g) {
		update();
		if (menu >= menus.length - 1 || menu < 0)
			return;
		
		g.drawImage(Main.rloop.menuBack, 0, 0, null);
		for (int x = 0; x <= menus[menu].tex.length - 1; x++) {
			if(menus[menu].tex[x] == null)
				break;
			g.drawImage(menus[menu].tex[x], 96, (x * 32) + 96, null);
			g.drawString(menus[menu].desc[x],menus[menu].tex[x].getWidth()+98 , (x * 32) + 110);
		
		
		}
	}

	public void handleClick(MouseEvent e) {
		int y = (e.getY()-96)/32;
		trigger(y);
	}
	private void trigger(int y){
		if(y > menus[menu].buttons.length || y < 0)
		return;	
		menus[menu].buttons[y].run();
		
	}
	public void update(){
		if(GameSettings.HQWater){
			menus[3].tex[waterbutton] = Main.MenuTextures[3];
			
		}
		else
			menus[3].tex[waterbutton] = Main.MenuTextures[4];
		
	}
}
