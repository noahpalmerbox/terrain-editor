package palmer.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JTextField;

import palmer.blocks.Block;
import palmer.main.GameEvents;
import palmer.main.GameMode;
import palmer.main.Main;
import palmer.res.LoadImages;

public class Console extends JFrame implements ActionListener {
	String String_commands[] = new String[10];
	String String_tcommands[] = new String[10];
	int int_commands, int_tcommands = 9;
	/**
	 * 
	 */
	StringTokenizer st;
	private static final long serialVersionUID = 1L;
	JTextField jtext = new JTextField("");

	public Console() {
		String_commands[0] = "null";
		String_commands[1] = "null";
		String_commands[2] = "null";
		String_commands[3] = "null";
		String_commands[4] = "null";
		String_commands[5] = "null";
		String_commands[6] = "null";
		String_commands[7] = "null";
		String_commands[8] = "null";
		String_commands[9] = "null";
		setTitle("Console");
		setSize(400, 100);
		setResizable(false);
		jtext.addActionListener(this);
		jtext.setActionCommand("text");
		addKeyListener(new ConsoleKeyBoard());
		add(jtext);
		setVisible(true);
	}

	public void print(String s) {
		jtext.setText("[Terrain Editor]" + s);
	}

	public void actionPerformed(ActionEvent arg0) {
		String s = jtext.getText();
		String_commands[0] = s;
		st = new StringTokenizer(s, ".");
		switch (st.nextToken()) {
		case "setBlock":
			String ss = st.nextToken();
			int i = Integer.parseInt(ss);
			Main.rloop.lv1.selb = Main.rloop.lv1.getBlock(i);
			Main.rloop.lv1.player.i.addItem(Main.rloop.lv1.getBlock(i), 9000);
			jtext.setText("");

			break;
		case "setTexture":
			Integer iob4 = new Integer(st.nextToken());
			Main.rloop.lv1.selb.Texture = Main.Textures[iob4];
			jtext.setText("");

			break;
		case "replace":
			String string = st.nextToken();

			Integer iob2 = new Integer(string);

			for (int x = 0; x < 16; x++) {
				for (int y = 0; y < 16; y++) {
					if (Main.rloop.lv1.getBlockGrid(x, y).id == iob2) {
						Main.rloop.lv1.changeBlock(x, y, Main.rloop.lv1.selb);
						jtext.setText("");
					}

				}
			}
			break;
		case "jump":
			String string2 = st.nextToken();

			Integer iob3 = new Integer(string2);
			Main.rloop.lv1.jumph = iob3;
			jtext.setText("");
			break;
		case "lag":
			String string4 = st.nextToken();

			Integer iob5 = new Integer(string4);
			Main.lag = iob5;
			jtext.setText("");
			break;
		case "editCon":
			String string5 = st.nextToken();

			Main.rloop.con = string5;
			jtext.setText("");
			break;
		case "antiLag":
			String t = st.nextToken();
			if(t.equals("on"))
				Main.rloop.antiLag = true;
			if(t.equals("off"))
				Main.rloop.antiLag = false;
			break;
		case "antiWater":
			GameEvents.fire("antiWater",Integer.parseInt(st.nextToken()),Integer.parseInt(st.nextToken()));
			break;
		case "instaTree":
			GameEvents.fire("instaTree", Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
			break;
		case "remove":
			String string6 = st.nextToken();
			switch(string6){
			case "lava":
				GameEvents.fire("removeLava");
				break;
			case "water":
				GameEvents.fire("removeWater");
				break;
			
			
			
			
			
			
			}
			Main.clear = true;
			
			break;
		case "GameEvent":
			GameEvents.fire(st.nextToken());
			
			
			
			break;
		case "GameEventxy":
			GameEvents.fire(st.nextToken(), Integer.parseInt(st.nextToken()),Integer.parseInt(st.nextToken()));
			
			
			
			break;
		case "GameMode":
			switch(st.nextToken()){
			case "s":
				Main.gm = GameMode.s;
				break;
			case "c":
				Main.gm = GameMode.c;
				break;
			
			
			
			
			
			}
			
			
			break;
		


		}

	}

	}


