package palmer.gui;

import java.awt.image.BufferedImage;

public class Menu {
	public BufferedImage[] tex;
	public MenuAction[] buttons;
	public String [] desc;
	public Menu(MenuAction ma [], BufferedImage bi[], String d[]){
		buttons = ma;
		tex = bi;
		desc = d;
	}
	public static Menu createMenu(MenuAction button[], BufferedImage tex[], String d[]){
		Menu newm = new Menu(button, tex, d);
		return newm;
	}
	public int addItem(MenuAction ma, BufferedImage bi, String s){
		for(int i = 0;i <= tex.length-1;i++){
			if(desc[i] == null){
				buttons[i] = ma;
				tex[i] = bi;
				desc[i] = s;
				return i;
			}
			
		}
	return -1;
	
	}

}
