package palmer.entity;

import java.awt.Graphics;
import java.awt.image.BufferedImage;


import palmer.blocks.Block;
import palmer.main.Main;


public abstract class Entity {
	public BufferedImage Texture;
	public int x, y;
	public String name = "Unnamed";
	public int id;
	public int Height;
	public Entity(int i, int eid, int x2, int y2){
		x = x2;
		y = y2;
		id = eid;
		Texture = Main.Textures[i];
	}
	public void Render(Graphics g, Block[][] blocks){
		
	}
}
