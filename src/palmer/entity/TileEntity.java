package palmer.entity;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import palmer.blocks.Block;
import palmer.levels.LevelType;
import palmer.main.GameEvents;
import palmer.main.Main;
import palmer.main.Orentation;
import palmer.res.Audio;

public abstract class TileEntity {
	public TileEntity(String a, String b, TileType t) {
		ActivateSound = a;
		DeactivateSound = b;
		type = t;
	}

	public TileEntity() {
		ActivateSound = "Beginning";
		DeactivateSound = "Beginning";
	}
	public TileEntity(TileType i) {
		ActivateSound = "Beginning";
		DeactivateSound = "Beginning";
	type = i;
	
	}

	public volatile boolean active = false;
	public volatile String ActivateSound;
	public volatile String DeactivateSound;
	public static TileEntity WarpPad = new TileEntity("Warp", "DeWarp", TileType.WarpPad) {
		public Block blocks[] = { Block.grass, Block.oreInside[Block.ORE_IRON], Block.sand};

		@Override
		public void Render(Graphics g) {
			g.drawImage(Main.rloop.menuBack, 0, 0, null);
			for (int count = 0; count < 5; count++) {
				if (count < blocks.length && blocks[count] != null)
					g.drawImage(blocks[count].Texture, 160, count * Main.rloop.TEXTURE_SIZE_Y + 160,
							null);
			}

		}

		@Override
		public void HandleClick(MouseEvent e) {
			if (e.getButton() == 1) {
				if (Main.rloop.lv1.bs.x / Main.rloop.TEXTURE_SIZE_X == 5) {
					if (Main.rloop.lv1.bs.y / Main.rloop.TEXTURE_SIZE_Y == 5) {
						Block[][]blocks= Main.rloop.lv1. blocks;
						BufferedImage b = Main.li.screen(Main.rloop.lv1.blocks);
						LevelType c = Main.rloop.lv1.CurrentLevelType;
						Main.rloop.lv1.init(LevelType.Normal);
						Main.rloop.addImage(Main.li.screen(b, Main.li.screen(Main.rloop.lv1.blocks), Orentation.Down),0,0, 0, -(16*Main.rloop.TEXTURE_SIZE_Y));
						

					}

					if (Main.rloop.lv1.bs.y / Main.rloop.TEXTURE_SIZE_Y == 6) {
						Block[][]blocks= Main.rloop.lv1. blocks;
						BufferedImage b = Main.li.screen(Main.rloop.lv1.blocks);
						LevelType c = Main.rloop.lv1.CurrentLevelType;
						Main.rloop.lv1.init(LevelType.Cave);
						Main.rloop.addImage(Main.li.screen(b, Main.li.screen(Main.rloop.lv1.getBlocks()), Orentation.Up),0,0, 0, (16*Main.rloop.TEXTURE_SIZE_Y));

					}
					if (Main.rloop.lv1.bs.y / Main.rloop.TEXTURE_SIZE_Y == 7) {
						Block[][]blocks= Main.rloop.lv1. blocks;
						BufferedImage b = Main.li.screen(Main.rloop.lv1.blocks);
						LevelType c = Main.rloop.lv1.CurrentLevelType;
						Main.rloop.lv1.init(LevelType.Sand);
						Main.rloop.addImage(Main.li.screen(b, Main.li.screen(Main.rloop.lv1.getBlocks()), Orentation.Up),0,0, 0, (16*Main.rloop.TEXTURE_SIZE_Y));

					}

				}
			}

		}

		public void update() {
			if (Main.rloop.lv1.getBlocks()[(Main.rloop.lv1.player.x + 2) / Main.rloop.TEXTURE_SIZE_X][Main.rloop.lv1.player.y / Main.rloop.TEXTURE_SIZE_Y + 1].id == 34
					|| Main.rloop.lv1.getBlocks()[(Main.rloop.lv1.player.x - 2) / Main.rloop.TEXTURE_SIZE_Y + 1][Main.rloop.lv1.player.y / Main.rloop.TEXTURE_SIZE_Y + 1].id == 34) {
				GameEvents.fire("warpPad");
		
			}else
				GameEvents.fire("endWarpPad");
		}

	};
	public static TileEntity Radio = new TileEntity("Song_1", "DeWarp", TileType.Radio) {

		@Override
		public void Render(Graphics g) {
			
			
		}

		@Override
		public void HandleClick(MouseEvent e) {
			
		}

		
		public void deactivate() {
			if (active) {
				active = false;
				Audio.stopCurrentSound();
				
			}
		}
		public void activate() {
			if (!active) {
				active = true;
				Audio.requestPlaySound(ActivateSound);
			}
		}
		@Override
		public void update() {
			if (Main.rloop.lv1.getBlocks()[(Main.rloop.lv1.player.x + 2) / Main.rloop.TEXTURE_SIZE_X][Main.rloop.lv1.player.y / Main.rloop.TEXTURE_SIZE_Y + 1].id == 35
					|| Main.rloop.lv1.getBlocks()[(Main.rloop.lv1.player.x - 2) / Main.rloop.TEXTURE_SIZE_X + 1][Main.rloop.lv1.player.y / Main.rloop.TEXTURE_SIZE_Y + 1].id == 35) {
				GameEvents.fire("radio");
		
			}else
				GameEvents.fire("endRadio");
			
		}
	};
	
	public TileType type;
	public abstract void Render(Graphics g);

	public abstract void HandleClick(MouseEvent e);
	public abstract void update();

	public void bind(Block b) {
		b.addTileEntity(this);
	}

	public void activate() {
		if (!active) {
			GameEvents.fire("banMapChanges");
			active = true;
			Audio.requestPlaySound(ActivateSound);
		}
	}

	public void deactivate() {
		if (active) {
			GameEvents.fire("allowMapChanges");
			active = false;
			Audio.requestPlaySound(DeactivateSound);
			
		}
	}

}
