package palmer.entity;

import java.awt.Graphics;

import palmer.blocks.Block;
import palmer.main.Main;

public class Slime extends Entity {

	public Slime(int x, int y) {
		super(12, 0, x, y);

	}

	public void Render(Graphics g, Block b[][]) {

		if (y != 0) {
			
			if (b[x][y - 1].id == 0) {
				y--;

			}
			
		}
		g.drawImage(Texture, x * 32, (y + 1) * 32 - 7, Main.rloop);
	}

}
