package palmer.blocks;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;

import palmer.entity.TileEntity;
import palmer.items.Item;
import palmer.items.ItemType;
import palmer.main.Main;
import palmer.res.LoadImages;

public class Block extends Item{
	public static int ORE_IRON = 0;
	public static int ORE_GOLD = 1;
	public static int ORE_DIAMOND = 2;
	public static BufferedImage Sheet;
	public static   Block air;
	public static   Block custom;
	public static   Block custom2;
	public static   Block custom3;
	public static   Block cloud;
	public static   Block stone;
	public static   Block dirt;
	public static   Block menuEmpty;
	public static   Block grass;
	public static   Block water;
	public static   Block bomb;
	public static  Block log;
	public static  Block branches;
	public static  Block stillWater;
	public static  Block nuke;
	public static  Block slimeBlock;
	public static  Block world;
	public static  Block tree;
	public static  Block bricks;
	public static  Block doorL;
	public static  Block doorR;
	public static  Block brickStairsR;
	public static  Block brickStairsL;
	public static  Block instaHouse;
	public static  Block bricksInside;
	public static  Block brickDoorL;
	public static  Block ladder;
	public static  Block lava;
	public static  Block brickDoorR;
	public static  Block stoneInside;
	public static Block oreInside[] = new Block[10];
	public static Block warpPad;
	public static Block radio;
	public static Block toolChest;
	public static Block deadBranches;
	public static Block sand;
	public static Block cactus;
	
	
	public static void init(){
		custom = new Block(-1).setSolid(false).setTexture(Main.Textures[10]).setType(ItemType.Block);
		custom2 = new Block(-2).setSolid(false).setTexture(Main.Textures[10]).setType(ItemType.Block);
		custom3 = new Block(-3).setSolid(false).setTexture(Main.Textures[10]).setType(ItemType.Block);
		air = new Block(0).setSolid(false).setTexture(Main.Textures[0]).setType(ItemType.Block);
		cloud = new Block(1).setSolid(false).setTexture(Main.Textures[1]).setType(ItemType.Block);
		stone = new Block(2).setSolid(true).setTexture(Main.Textures[2]).setType(ItemType.Block);
		dirt = new Block(3).setSolid(true).setTexture(Main.Textures[3]).setType(ItemType.Block);
		menuEmpty = new Block(4).setSolid(true).setTexture(Main.Textures[4]).setType(ItemType.Block);
		grass = new Block(5).setSolid(true).setTexture(Main.Textures[5]).setType(ItemType.Block);
		water = new BlockWater(6).setSolid(false).setTexture(Main.Textures[6]).setType(ItemType.Block);
		bomb = new BlockBomb(7).setSolid(false).setTexture(Main.Textures[7]).setType(ItemType.Block);
		 log = new Block(8).setSolid(true).setTexture(Main.Textures[8]).setType(ItemType.Block);
		 branches = new Block(9).setSolid(true).setTexture(Main.Textures[9]).setType(ItemType.Block);
		  stillWater = new Block(10).setSolid(false).setTexture(Main.Textures[6]).setType(ItemType.Block);
		  nuke = new Block(11).setSolid(false).setTexture(Main.Textures[11]).setType(ItemType.Block);
		  slimeBlock = new Block(12).setSolid(true).setTexture(Main.Textures[12]).setType(ItemType.Block);
		  world = new Block(13).setSolid(false).setTexture(Main.Textures[13]).setType(ItemType.Block);
		  tree = new Block(14).setSolid(false).setTexture(Main.Textures[14]).setType(ItemType.Block);
		  bricks = new Block(15).setSolid(true).setTexture(Main.Textures[15]).setType(ItemType.Block);
		  doorL = new Block(16).setSolid(false).setTexture(Main.Textures[16]).setType(ItemType.Block);
		 doorR = new Block(17).setSolid(false).setTexture(Main.Textures[17]).setType(ItemType.Block);
		  brickStairsR = new Block(18).setSolid(true).setTexture(Main.Textures[18]).setType(ItemType.Block);
		  brickStairsL = new Block(19).setSolid(true).setTexture(Main.Textures[19]).setType(ItemType.Block);
		 instaHouse = new Block(20).setSolid(false).setTexture(Main.Textures[20]).setType(ItemType.Block);
		 bricksInside = new Block(21).setSolid(false).setTexture(Main.Textures[21]).setType(ItemType.Block);
		 brickDoorL = new Block(22).setSolid(false).setTexture(Main.Textures[22]).setType(ItemType.Block);
		 ladder = new Block(23).setSolid(false).setTexture(Main.Textures[23]).setType(ItemType.Block);
		 lava = new Block(24).setSolid(false).setTexture(Main.Textures[25]).setType(ItemType.Block);
		 brickDoorR = new Block(26).setSolid(false).setTexture(Main.Textures[24]).setType(ItemType.Block);
		 stoneInside = new Block(30).setSolid(false).setTexture(Main.Textures[26]).setType(ItemType.Block);
		 oreInside[0] = new Block(31).setSolid(false).setTexture(Main.Textures[27]).setType(ItemType.Block);
		 oreInside[1] = new Block(32).setSolid(false).setTexture(Main.Textures[28]).setType(ItemType.Block);
		 oreInside[2] = new Block(33).setSolid(false).setTexture(Main.Textures[29]).setType(ItemType.Block);
		 warpPad = new Block(34).setSolid(true).setTexture(Main.Textures[30]).setType(ItemType.Block);
		 radio = new Block(35).setSolid(true).setTexture(Main.Textures[31]).setType(ItemType.Block);
		 toolChest  = new Block(36).setSolid(true).setTexture(Main.Textures[32]).setType(ItemType.Block);
		 deadBranches = new Block(37).setSolid(true).setTexture(Main.Textures[33]).setType(ItemType.Block);
		 sand = new Block(38).setSolid(true).setTexture(Main.Textures[34]).setType(ItemType.Block);
		 cactus = new Block(39).setSolid(true).setTexture(Main.Textures[35]).setType(ItemType.Block);
		 
		 
		 
		
	}
	public boolean solid = true;
	public TileEntity entity;
	String Name = "Unnamed";
	public int waterCount = 2;

	
	public Block(int id2){
		super(id2);
				
	}

	public boolean hasEntity(){
		if(entity == null)
			return false;
		else
			return true;
	}
	public void addTileEntity(TileEntity e){
		entity = e;
		
	}
	public Block setSolid(boolean b){
		solid = b;
		return this;
	}
	public Block setTexture(BufferedImage b){
		Texture = b;
		return this;
	}
	public Block setType(ItemType i){
		type = i;
		return this;
		
	}
	
	
	
}
